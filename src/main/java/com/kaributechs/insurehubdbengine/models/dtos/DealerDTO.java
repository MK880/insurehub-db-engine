package com.kaributechs.insurehubdbengine.models.dtos;

import lombok.Data;

import java.util.Set;

@Data
public class DealerDTO {

    private Long id;
    private String username;
    private Set<DealerServiceDTO> services;
    private boolean available;
}
