package com.kaributechs.insurehubdbengine.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class GetCompanyTemplateIdDTO {
    private String templateName;
    private Long companyId;
}
