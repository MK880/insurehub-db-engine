package com.kaributechs.insurehubdbengine.models.dtos;

import lombok.Data;

@Data
public class ServiceDTO {
    private long id;
    private String name;
}
