package com.kaributechs.insurehubdbengine.models.dtos;

import lombok.NoArgsConstructor;

import java.util.Date;


@NoArgsConstructor
public class FileUploadResponseDTO {
    private Long id;

    private String name;

    private String fileType;

    private String processId;

    private String type;

    private Date uploadDate;

    private String downloadUrl;

    private long size;

    public FileUploadResponseDTO(Long id, String name, String fileType, String processId, String type, Date uploadDate, String downloadUrl, long size) {
        this.id = id;
        this.name = name;
        this.fileType = fileType;
        this.processId = processId;
        this.type = type;
        this.uploadDate = uploadDate;
        this.downloadUrl = downloadUrl;
        this.size = size;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }
}
