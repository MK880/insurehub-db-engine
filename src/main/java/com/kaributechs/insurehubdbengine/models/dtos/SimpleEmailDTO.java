package com.kaributechs.insurehubdbengine.models.dtos;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@NoArgsConstructor
@ToString
@AllArgsConstructor
public class SimpleEmailDTO {
    private String recipient;
    private String subject;
    private String body;
    private String cc;
    private String bcc;
    private String staticAttachments;
    private MultipartFile[] attachments;


}
