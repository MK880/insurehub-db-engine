package com.kaributechs.insurehubdbengine.models.dtos;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class UpdateStaticAttachmentDTO {
    private Long id;
    private String name;
    private MultipartFile attachment;
}
