package com.kaributechs.insurehubdbengine.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AddStaticAttachmentDTO {
    private Long companyId;
    private MultipartFile attachment;
    private String name;
}
