package com.kaributechs.insurehubdbengine.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Template {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String fileName;
    private String headerImageUrl;
    private String footerImageUrl;
    private String signature;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "company_id",nullable = false)
    @JsonBackReference("company_template")
    Company company;



}
