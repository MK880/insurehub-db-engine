package com.kaributechs.insurehubdbengine.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.minidev.json.annotate.JsonIgnore;

import javax.validation.constraints.NotNull;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InsurehubInsuranceDTO {

    @NotNull(message = "name cannot be null")
    private String name;

    @NotNull(message = "key cannot be null")
    private String key;

}
