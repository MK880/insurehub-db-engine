package com.kaributechs.insurehubdbengine.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class StaticAttachment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String url;
    private String fileExtension;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "company_id",nullable = false)
    @JsonBackReference("company_static_attachment")
    Company company;
}
