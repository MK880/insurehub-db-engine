package com.kaributechs.insurehubdbengine.models.dealer;

import lombok.Data;

@Data
public class RegisterDealerDTO {
    private String username;
    private String companyName;
    private String mobileNumber;
    private String email;
    private boolean available;
}
