package com.kaributechs.insurehubdbengine.models.dealer;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
public class DealerRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private boolean accident;
    private boolean towing;
    private boolean fixedCar;
    private boolean towedCar;
    private boolean contactedDealer;
    private boolean cancelledRequest;
    private String processId;
    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private DriverDetails driverDetails;
    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private ServiceDetails serviceDetails;
    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private VehicleDetails vehicleDetails;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private Service requestedService;


    @OneToMany(mappedBy = "dealerRequest",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("dealerRequest_quotation")
    private Set<Quotation> quotations;



    @Override
    public String toString() {
        return "DealerRequest{" +
                "id=" + id +
                ", accident=" + accident +
                ", towing=" + towing +
                ", processId='" + processId + '\'' +
                ", driverDetails=" + driverDetails +
                ", serviceDetails=" + serviceDetails +
                ", vehicleDetails=" + vehicleDetails +
                ", quotations=" + quotations +
                '}';
    }
}
