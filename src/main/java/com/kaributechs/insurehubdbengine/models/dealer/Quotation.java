package com.kaributechs.insurehubdbengine.models.dealer;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Quotation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private BigDecimal price;

    private String serviceName;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("dealerRequest_quotation")
    private DealerRequest dealerRequest;

    @ManyToOne(fetch = FetchType.EAGER)
//    @JsonBackReference("dealer_quotation")
    private Dealer dealer;


}
