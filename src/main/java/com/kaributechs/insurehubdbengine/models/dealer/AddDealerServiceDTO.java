package com.kaributechs.insurehubdbengine.models.dealer;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class AddDealerServiceDTO {
    private Long serviceId;
    private BigDecimal price;
}
