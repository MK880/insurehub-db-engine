package com.kaributechs.insurehubdbengine.models.claims;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class ClaimVehicleDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String vehicleMake;
    private String vehicleModel;
    private String vehicleModelYear;
    private String vehicleRegistrationNumber;
    private String vehicleMileage;
    private String vehicleUsage;
}
