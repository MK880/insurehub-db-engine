package com.kaributechs.insurehubdbengine.models.claims.dtos;

import lombok.*;

import java.util.Date;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

public class ClaimsWorkflowRequestDTO  {

    private boolean acceptDeclaration;
    private Date claimCreationDate;
    private String insuranceType;
    private Long companyId;

    private ClaimClientDetailsDTO clientDetails;
    private ClaimInsuredDetailsDTO insuredDetails;
    private ClaimVehicleDetailsDTO vehicleDetails;
    private ClaimDriverDetailsDTO driverDetails;
    private ClaimAccidentDetailsDTO accidentDetails;
    private ClaimEvidenceDetailsDTO evidenceDetails;
    private String documentSourceChannel;
}
