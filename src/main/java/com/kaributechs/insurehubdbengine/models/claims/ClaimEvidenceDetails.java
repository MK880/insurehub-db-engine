package com.kaributechs.insurehubdbengine.models.claims;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class ClaimEvidenceDetails {
    private MultipartFile[] policeReport;
    private MultipartFile[] videoEvidence;
    private MultipartFile[] audioEvidence;
    private MultipartFile[] additionalDocuments;
}
