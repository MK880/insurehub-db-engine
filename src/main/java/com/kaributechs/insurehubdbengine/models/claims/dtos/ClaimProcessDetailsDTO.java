package com.kaributechs.insurehubdbengine.models.claims.dtos;

import lombok.Data;

@Data
public class ClaimProcessDetailsDTO {
    private String team;
    private Double claimAmount;
    private boolean approved;
    private boolean autoAssess;
    private boolean getMoreDetails;
    private boolean getPanelBeater;
    private String claimStatus;
}
