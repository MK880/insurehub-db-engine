package com.kaributechs.insurehubdbengine.models.claims;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class ClaimAccidentDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String driversCondition;
    private String accidentHistory;
    private String accidentHistoryDetails;
    private String driversConditionDetails;
    private String vehiclePurpose;
    private String natureOfInjuries;
    private String accidentDescription;
}
