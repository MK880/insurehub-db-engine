package com.kaributechs.insurehubdbengine.models.claims;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
public class ClaimDriverDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String driversFirstName;
    private String driversLastName;
    private String driversEmail;
    private String driversPhoneNumber;
    private String driversResidentialStreetAddress;
    private String driversSuburb;
    private String driversResidentialCity;
    private String driversRegion;
    private String driversCountry;
    private String driversZipCode;
    private String driversWorkingStreetAddress;
    private String driversWorkingSuburb;
    private String driversWorkingCity;
    private String driversWorkingRegion;
    private String driversWorkingCountry;
    private String driversWorkingZipCode;
    private String driversLicenseNumber;
    private Date dateOfLicenseIssue;
}
