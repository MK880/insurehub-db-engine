package com.kaributechs.insurehubdbengine.models.claims;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class ClaimInsuredDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String insuredId;
    private String insuredSalutation;
    private String insuredFirstName;
    private String insuredLastName;
    private String insuredPhoneNumber;
    private String insuredEmail;
    private String insuredResidentialStreetAddress;
    private String insuredSuburb;
    private String insuredResidentialCity;
    private String insuredRegion;
    private String insuredCountry;
    private String insuredZipCode;
    private String insuredWorkingStreetAddress;
    private String insuredWorkingSuburb;
    private String insuredWorkingCity;
    private String insuredWorkingRegion;
    private String insuredWorkingCountry;
    private String insuredWorkingZipCode;
}
