package com.kaributechs.insurehubdbengine.models.claims;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Data
@Entity
public class ClaimClientDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long clientId;
    private String clientFirstName;
    private String clientLastName;

    @NotNull(message="Personal details cannot be null")
    private String clientEmail;
    private String clientPhoneNumber;
    private String clientIdNumber;
    private String clientType;
    private String businessName;
    private String businessRegistrationNumber;
}
