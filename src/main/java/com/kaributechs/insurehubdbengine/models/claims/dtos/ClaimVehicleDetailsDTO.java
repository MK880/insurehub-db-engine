package com.kaributechs.insurehubdbengine.models.claims.dtos;

import lombok.Data;

@Data
public class ClaimVehicleDetailsDTO {
    private String vehicleMake;
    private String vehicleModel;
    private String vehicleModelYear;
    private String vehicleRegistrationNumber;
    private String vehicleMileage;
    private String vehicleUsage;
}
