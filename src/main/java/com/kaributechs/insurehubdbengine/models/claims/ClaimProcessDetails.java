package com.kaributechs.insurehubdbengine.models.claims;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class ClaimProcessDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String initiator;
    private String team;
    private Double claimAmount;
    private boolean approved;
    private boolean autoAssess;
    private boolean getMoreDetails;
    private boolean getPanelBeater;
    private String claimStatus;
}
