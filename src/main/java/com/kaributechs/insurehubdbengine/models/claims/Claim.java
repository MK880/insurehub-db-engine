package com.kaributechs.insurehubdbengine.models.claims;

import com.kaributechs.insurehubdbengine.utilities.StringPrefixedSequenceIdGenerator;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
@AllArgsConstructor
public class Claim {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "claim_seq")
    @GenericGenerator(
            name = "claim_seq",
            strategy = "com.kaributechs.insurehubdbengine.utilities.StringPrefixedSequenceIdGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.INCREMENT_PARAM, value = "1"),
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.VALUE_PREFIX_PARAMETER, value = "C"),
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%05d") })
    private String id;
    @NotNull(message = "Customer username cannot be null")
    private String username;
    private String processInstanceId;
    private Date claimCreationDate;
    private Date lastClaimStatusChangeDate;
    private String insuranceType;
    private String status;
    private Long companyId;
    private boolean deleted;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    ClaimProcessDetails processDetails;
    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    ClaimClientDetails clientDetails;
    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    ClaimInsuredDetails insuredDetails;
    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    ClaimVehicleDetails vehicleDetails;
    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    ClaimDriverDetails driverDetails;
    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    ClaimAccidentDetails accidentDetails;

}
