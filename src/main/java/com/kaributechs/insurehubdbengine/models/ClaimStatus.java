package com.kaributechs.insurehubdbengine.models;

public enum ClaimStatus {
    REQUESTED,
    PROCESSED,
    REJECTED
}
