package com.kaributechs.insurehubdbengine.models.insurance_quotation;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RequestQuoteDTO {

    @NotNull(message = "insuranceId cannot be null")
    private Long insuranceId;
    private String email;
    private String mobileNumber;
    private boolean sendEmail;

}
