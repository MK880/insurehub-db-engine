package com.kaributechs.insurehubdbengine.models.insurance_quotation;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NewBusinessClientQuote {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private ClientQuotationCompanyDetails companyDetails;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private ClientQuotationPremiumDetails premiumDetails;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("clientQuotation_quotations")
    private NewBusinessClientQuotation clientQuotations;

}
