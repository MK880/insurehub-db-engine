package com.kaributechs.insurehubdbengine.models.insurance_quotation;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.kaributechs.insurehubdbengine.models.insurance_quotation.NewBusinessClientQuote;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NewBusinessClientQuotation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @CreationTimestamp
    private Date date;
    private String insurance;

    @OneToMany(mappedBy = "clientQuotations",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("clientQuotation_quotations")
    private Set<NewBusinessClientQuote> quotations;


}
