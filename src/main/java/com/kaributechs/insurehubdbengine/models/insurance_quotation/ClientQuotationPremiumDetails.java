package com.kaributechs.insurehubdbengine.models.insurance_quotation;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClientQuotationPremiumDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private BigDecimal principalMemberPremium;
    private BigDecimal childBeneficiaryPremium;
    private BigDecimal adultBeneficiaryPremium;
    private BigDecimal seniorBeneficiaryPremium;
}
