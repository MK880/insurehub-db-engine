package com.kaributechs.insurehubdbengine.models.new_business.company_vehicle_insurance;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class CompanyInsuranceQuestions {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String carInsuranceType;
    private String thirdPartyLiability;
    private String allRisksYesOrNo;
    private String replacementCarYesOrNo;
    private String totalLossYesOrNo;
    private String excessYesOrNo;
    private String periodOfInsuranceTo;
    private String periodOfInsuranceFrom;
    private String maritalStatus;
}
