package com.kaributechs.insurehubdbengine.models.new_business.household_insurance;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class HouseholdInsurancePropertyDetailsDTO {

    private boolean isOccupiedByFamilyOnly;
    private String occupationDescription;
    private String propertyUse;
    private String isLetToAFamily;
    private String numberOfTenants;
    private String isBuiltWithBricks;
    private String materialsUsedToBuild;
    private String isRoofedWithSlateOrTiles;
    private String materialsUsedToRoof;
    private String propertyHasPreservationOrder;
    private String preservationOrderDescription;
    private String propertyHasUniqueFeatures;
    private String uniqueFeatures;
    private String yearHouseWasBuilt;
    private String areaOfProperty;
    private String areaOfPropertyDetails;


    @NotNull(message = "propertyAddress cannot be null")
    private HouseholdInsurancePropertyAddressDTO propertyAddress;

    @NotNull(message = "protectionDetails cannot be null")
    private HouseholdInsuranceProtectionDetailsDTO protectionDetails;




}
