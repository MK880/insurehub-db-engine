package com.kaributechs.insurehubdbengine.models.new_business.personal_vehicle_insurance;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
public class PersonalDetailsDTO {
    private String salutation;
    private String nationalId;
    private String clientFirstName;
    private String clientLastName;
    private String dateOfBirth;
    private String clientGender;
    private String maritalStatus;
    private String identificationType;
}
