package com.kaributechs.insurehubdbengine.models.new_business.company_vehicle_insurance;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.kaributechs.insurehubdbengine.models.new_business.VehicleUserDetails;
import com.kaributechs.insurehubdbengine.models.new_business.company_vehicle_insurance.CompanyVehicleInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.company_vehicle_insurance.OtherVehicleUserDetails;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class CompanyInsuranceVehicleDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String registrationNumber;
    private String make;
    private String year;
    private String type;
    private String mileage;
    private String worth;
    private String usage;
    private String numberOfSeats;
    private String alarmSystemYesOrNo;
    private String nightParkingYesOrNo;
    private boolean isVehicleUsedByOthers;
    private String takenHome;

    @OneToOne(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    private VehicleUserDetails vehicleUser;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("companyVehicleInsurance_vehicles")
    private CompanyVehicleInsurance companyVehicleInsurance;

    @OneToMany(mappedBy = "vehicleDetails",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("vehicleDetails_otherVehicleUserDetails")
    private List<OtherVehicleUserDetails> otherVehicleUsers;

}
