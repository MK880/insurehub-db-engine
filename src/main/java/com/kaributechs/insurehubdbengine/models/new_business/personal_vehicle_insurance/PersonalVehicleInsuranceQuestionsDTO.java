package com.kaributechs.insurehubdbengine.models.new_business.personal_vehicle_insurance;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
public class PersonalVehicleInsuranceQuestionsDTO {

    private String carInsuranceType;
    private String thirdPartyLiability;
    private String bearsAllRisks;
    private String interestedInCourtesyCar;
    private String coversTotalLoss;
    private String coversExcessCosts;
    private String periodOfInsuranceTo;
    private String periodOfInsuranceFrom;
}
