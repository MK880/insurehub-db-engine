package com.kaributechs.insurehubdbengine.models.new_business.life_insurance;

import com.kaributechs.insurehubdbengine.models.new_business.ContactDetailsDTO;
import com.kaributechs.insurehubdbengine.models.new_business.SpouseDetailsDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
public class LifeInsuranceDTO {
    @NotNull(message="companyId cannot be null")
    private Long companyId;

    @NotNull(message = "processId cannot be null")
    private String processId;

    @NotNull(message = "memberDetails cannot be null")
    private LifeInsuranceMemberDetailsDTO memberDetails;

    @NotNull(message = "contactDetails cannot be null")
    private ContactDetailsDTO contactDetails;

    @NotNull(message = "spouse cannot be null")
    private SpouseDetailsDTO spouse;


    private Set<LifeInsuranceBeneficiaryDetailsDTO> beneficiaries;


    private Set<LifeInsuranceExtendedFamilyDetailsDTO> extendedFamily;

    @NotNull(message = "insuranceQuestions cannot be null")
    private LifeInsuranceQuestionsDTO insuranceQuestions;

    @NotNull(message = "otherInsuranceDetails cannot be null")
    private OtherInsuranceDetailsDTO otherInsuranceDetails;


}
