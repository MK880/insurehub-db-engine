package com.kaributechs.insurehubdbengine.models.new_business.household_insurance;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class HouseholdInsuranceProtectionDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private int daysLeftUnoccupied;
    private boolean isLeftUnoccupiedAtDay;
    private boolean hasDeadlockDevices;
    private boolean isFittedWithSecureLocks;
    private boolean hasBurglarAlarm;
    private boolean isMonitoredAllDay;
    private boolean hasSmokeDetectors;
}
