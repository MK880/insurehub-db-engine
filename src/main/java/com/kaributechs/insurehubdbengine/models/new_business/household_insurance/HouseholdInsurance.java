package com.kaributechs.insurehubdbengine.models.new_business.household_insurance;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.new_business.ContactDetails;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class HouseholdInsurance {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String processId;


    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private HouseholdInsuranceMemberDetails memberDetails;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private ContactDetails contactDetails;

    @OneToMany(mappedBy = "householdInsurance",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("householdInsurance_properties")
    private Set<HouseholdInsurancePropertyDetails> properties;

    @OneToMany(mappedBy = "householdInsurance",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("householdInsurance_children")
    private Set<HouseholdInsuranceChildrenDetails> children;

    @OneToMany(mappedBy = "householdInsurance",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("householdInsurance_extendedFamily")
    private Set<HouseholdInsuranceExtendedFamilyDetails> extendedFamily;

    @OneToMany(mappedBy = "householdInsurance",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("householdInsurance_beneficiaries")
    private Set<HouseholdInsuranceBeneficiaryDetails> beneficiaries;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private HouseholdInsuranceQuestions insuranceQuestions;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("company_householdInsuranceClients")
    private Company company;
}
