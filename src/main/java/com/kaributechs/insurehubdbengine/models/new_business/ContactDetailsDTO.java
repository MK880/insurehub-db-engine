package com.kaributechs.insurehubdbengine.models.new_business;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
public class ContactDetailsDTO {

    private String emailAddress;
    private String phoneNumber;
    private String residentialStreetAddress;
    private String residentialCity;
    private String region;
    private String residentialZip;
    private String workAddressZip;
    private String country;
    private String workStreetAddress;
    private String workCity;
    private String zip;
    private String workRegion;
    private String workCountry;
    private String suburb;
    private String workSuburb;
}
