package com.kaributechs.insurehubdbengine.models.new_business.personal_vehicle_insurance;

import com.kaributechs.insurehubdbengine.models.new_business.ContactDetailsDTO;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
public class PersonalVehicleInsuranceDTO {

    @NotNull(message="companyId cannot be null")
    private Long companyId;

    @NotNull(message="processId cannot be null")
    private String processId;

    @NotNull(message="personalDetails cannot be null")
    private PersonalDetailsDTO personalDetails;

    @NotNull(message="contactDetails cannot be null")
    private ContactDetailsDTO contactDetails;

    private Set<PersonalVehicleInsuranceVehicleDetailsDTO> vehicles;

    @NotNull(message="insuranceQuestions cannot be null")
    private PersonalVehicleInsuranceQuestionsDTO insuranceQuestions;

}
