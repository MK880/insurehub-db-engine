package com.kaributechs.insurehubdbengine.models.new_business.life_insurance;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class LifeInsuranceBeneficiaryDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String lastName;
    private String firstName;
    private String salutation;
    private String nationalId;
    private Date dateOfBirth;
    private String emailAddress;
    private String phoneNumber;
    private String gender;
    private String maritalStatus;
    private String identificationType;
    private String relationshipToMember;
    private String proceeds;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("lifeInsurance_beneficiaries")
    private LifeInsurance lifeInsurance;
}
