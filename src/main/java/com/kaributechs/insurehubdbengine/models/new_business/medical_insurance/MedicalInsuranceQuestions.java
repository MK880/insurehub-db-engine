package com.kaributechs.insurehubdbengine.models.new_business.medical_insurance;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class MedicalInsuranceQuestions {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String insuranceType;
    private String insurancePremiumRange;
    private String paymentFrequency;
    private String insuranceCurrency;
    private String condolenceFee;
    private String excessYesOrNo;
    private String extendedFamilyBenefits;
    private String periodOfInsuranceFrom;
    private String maritalStatus;
}
