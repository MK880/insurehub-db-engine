package com.kaributechs.insurehubdbengine.models.new_business.personal_vehicle_insurance;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.new_business.ContactDetails;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
@Entity
public class PersonalVehicleInsurance {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String processId;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private PersonalDetails personalDetails;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private ContactDetails contactDetails;

    @OneToMany(mappedBy = "personalVehicleInsurance",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("personalVehicleInsurance_vehicles")
    private Set<PersonalVehicleInsuranceVehicleDetails> vehicles;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private PersonalVehicleInsuranceQuestions insuranceQuestions;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("company_personalVehicleInsuranceClients")
    private Company company;

}
