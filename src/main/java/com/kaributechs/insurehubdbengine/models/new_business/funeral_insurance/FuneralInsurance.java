package com.kaributechs.insurehubdbengine.models.new_business.funeral_insurance;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.new_business.ContactDetails;
import com.kaributechs.insurehubdbengine.models.new_business.SpouseDetails;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class FuneralInsurance {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String processId;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private FuneralInsuranceMemberDetails memberDetails;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private ContactDetails contactDetails;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private SpouseDetails spouse;

    @OneToMany(mappedBy = "funeralInsurance",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("funeralInsurance_children")
    private Set<FuneralInsuranceChildrenDetails> children;

    @OneToMany(mappedBy = "funeralInsurance",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("funeralInsurance_extendedFamily")
    private Set<FuneralInsuranceExtendedFamilyDetails> extendedFamily;

    @OneToMany(mappedBy = "funeralInsurance",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("funeralInsurance_beneficiaries")
    private Set<FuneralInsuranceBeneficiaryDetails> beneficiaries;


    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private FuneralInsuranceQuestions insuranceQuestions;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("company_funeralInsuranceClients")
    private Company company;
}
