package com.kaributechs.insurehubdbengine.models.new_business.household_insurance;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
public class HouseholdInsuranceQuestions {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private BigDecimal fullCostOfHome;
    private BigDecimal fullReplacementCostOfContents;
    private boolean fireOnlyCover;
    private boolean fullAccidentalDamageCover;
    private boolean optionalCoverForJewellery;
    private Date periodOfInsuranceFrom;
    private Date periodOfInsuranceTo;
    private boolean optionalCoverOnIndividualItems;
}
