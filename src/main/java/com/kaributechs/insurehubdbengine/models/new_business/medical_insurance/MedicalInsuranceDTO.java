package com.kaributechs.insurehubdbengine.models.new_business.medical_insurance;


import com.kaributechs.insurehubdbengine.models.new_business.ContactDetailsDTO;
import com.kaributechs.insurehubdbengine.models.new_business.SpouseDetailsDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@Data
public class MedicalInsuranceDTO {
    @NotNull(message="companyId cannot be null")
    private Long companyId;

    private String processId;

    @NotNull(message="memberDetails cannot be null")
    private MedicalInsuranceMemberDetailsDTO memberDetails;

    @NotNull(message="contactDetails cannot be null")
    private ContactDetailsDTO contactDetails;

    private SpouseDetailsDTO spouse;

    private Set<MedicalInsuranceChildrenDetailsDTO> children;

    private Set<MedicalInsuranceExtendedFamilyDetailsDTO> extendedFamily;

    private Set<MedicalInsuranceBeneficiaryDetailsDTO> beneficiaries;

    @NotNull(message="insuranceQuestions cannot be null")
    private MedicalInsuranceQuestionsDTO insuranceQuestions;

}
