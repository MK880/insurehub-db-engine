package com.kaributechs.insurehubdbengine.models.new_business.life_insurance;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Data
public class LifeInsuranceQuestionsDTO {

    private String insuranceType;
    private String benefits;
    private String products;
    private String whenCoverToStart;
    private String premiumFrequency;
    private String grossIncome;
    private String policyObjective;
    private String doInsuredSmoke;
    private String offense;
    private String active;


}
