package com.kaributechs.insurehubdbengine.models.new_business.medical_insurance;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.new_business.ContactDetails;
import com.kaributechs.insurehubdbengine.models.new_business.SpouseDetails;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class MedicalInsurance {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String processId;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private MedicalInsuranceMemberDetails memberDetails;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private ContactDetails contactDetails;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private SpouseDetails spouse;

    @OneToMany(mappedBy = "medicalInsurance",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("medicalInsurance_children")
    private Set<MedicalInsuranceChildrenDetails> children;

    @OneToMany(mappedBy = "medicalInsurance",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("medicalInsurance_extendedFamily")
    private Set<MedicalInsuranceExtendedFamilyDetails> extendedFamily;

    @OneToMany(mappedBy = "medicalInsurance",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("medicalInsurance_beneficiaries")
    private Set<MedicalInsuranceBeneficiaryDetails> beneficiaries;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private MedicalInsuranceQuestions insuranceQuestions;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("company_medicalInsuranceClients")
    private Company company;

}
