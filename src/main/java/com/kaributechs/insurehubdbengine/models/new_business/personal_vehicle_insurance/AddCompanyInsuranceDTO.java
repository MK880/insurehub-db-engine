package com.kaributechs.insurehubdbengine.models.new_business.personal_vehicle_insurance;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class AddCompanyInsuranceDTO {

    @NotNull(message = "InsuranceId cannot be null")
    private Long companyId;
    @NotNull(message = "InsuranceId cannot be null")
    private Long insuranceId;
    @NotNull(message = "principalMemberPremium cannot be null")
    private BigDecimal principalMemberPremium;
//    @NotNull(message = "childBeneficiaryPremium cannot be null")
    private BigDecimal childBeneficiaryPremium;
//    @NotNull(message = "adultBeneficiaryPremium cannot be null")
    private BigDecimal adultBeneficiaryPremium;
//    @NotNull(message = "seniorBeneficiaryPremium cannot be null")
    private BigDecimal seniorBeneficiaryPremium;


}
