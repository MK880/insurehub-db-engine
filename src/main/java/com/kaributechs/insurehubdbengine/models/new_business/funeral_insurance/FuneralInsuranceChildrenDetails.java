package com.kaributechs.insurehubdbengine.models.new_business.funeral_insurance;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.kaributechs.insurehubdbengine.models.new_business.life_insurance.LifeInsurance;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class FuneralInsuranceChildrenDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String lastName;
    private String firstName;
    private String salutation;
    private String nationalId;
    private Date dateOfBirth;
    private String emailAddress;
    private String phoneNumber;
    private String gender;
    private String identificationType;
    private String relationshipToMember;
    private String funeralCover;
    private String currency;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("funeralInsurance_children")
    private FuneralInsurance funeralInsurance;
}
