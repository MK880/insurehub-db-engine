package com.kaributechs.insurehubdbengine.models.new_business.funeral_insurance;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.kaributechs.insurehubdbengine.models.new_business.ContactDetails;
import com.kaributechs.insurehubdbengine.models.new_business.ContactDetailsDTO;
import com.kaributechs.insurehubdbengine.models.new_business.SpouseDetails;
import com.kaributechs.insurehubdbengine.models.new_business.SpouseDetailsDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class FuneralInsuranceDTO {
    @NotNull(message="companyId cannot be null")
    private Long companyId;

    @NotNull(message = "processId cannot be null")
    private String processId;

    @NotNull(message = "memberDetails cannot be null")
    private FuneralInsuranceMemberDetailsDTO memberDetails;

    @NotNull(message = "contactDetails cannot be null")
    private ContactDetailsDTO contactDetails;

    private SpouseDetailsDTO spouse;


    private Set<FuneralInsuranceChildrenDetailsDTO> children;


    private Set<FuneralInsuranceExtendedFamilyDetailsDTO> extendedFamily;


    private Set<FuneralInsuranceBeneficiaryDetailsDTO> beneficiaries;

    @NotNull(message = "insuranceQuestions cannot be null")
    private FuneralInsuranceQuestionsDTO insuranceQuestions;
}
