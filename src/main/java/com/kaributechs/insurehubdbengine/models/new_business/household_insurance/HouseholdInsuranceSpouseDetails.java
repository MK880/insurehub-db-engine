package com.kaributechs.insurehubdbengine.models.new_business.household_insurance;

import java.util.Date;

public class HouseholdInsuranceSpouseDetails {
    private String lastName;
    private String firstName;
    private String salutation;
    private String nationalId;
    private Date dateOfBirth;
    private String emailAddress;
    private String phoneNumber;
    private String gender;
    private String relationshipToBeneficiary;
    private String identificationType;
}
