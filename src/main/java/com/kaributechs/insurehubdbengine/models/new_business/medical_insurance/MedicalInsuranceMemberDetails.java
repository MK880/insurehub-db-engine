package com.kaributechs.insurehubdbengine.models.new_business.medical_insurance;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class MedicalInsuranceMemberDetails  {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String lastName;
    private String firstName;
    private String salutation;
    private String nationalId;
    private Date dateOfBirth;
    private String gender;
    private String maritalStatus;
    private String identificationType;
}
