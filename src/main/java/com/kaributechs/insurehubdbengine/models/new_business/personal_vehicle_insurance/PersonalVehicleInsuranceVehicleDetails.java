package com.kaributechs.insurehubdbengine.models.new_business.personal_vehicle_insurance;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.kaributechs.insurehubdbengine.models.new_business.medical_insurance.MedicalInsurance;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@Getter
@Setter
@Entity
public class PersonalVehicleInsuranceVehicleDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String vehicleRegistrationNumber;
    private String clientLicenseNumber;
    private String dateWhenLicenseWasObtained;
    private String vehicleMake;
    private String vehicleModel;
    private String vehicleYear;
    private String vehicleMileage;
    private String vehicleWorth;
    private String numberOfSeats;
    private String vehicleHasAlarmSystem;
    private String residentHasNightParking;
    private String numberOfDrivers;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("personalVehicleInsurance_vehicles")
    private PersonalVehicleInsurance personalVehicleInsurance;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private PersonalInsuranceSecondaryDriverDetails secondaryDriverDetails;
}
