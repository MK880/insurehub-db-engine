package com.kaributechs.insurehubdbengine.models.new_business.company_vehicle_insurance;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.new_business.*;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class CompanyVehicleInsurance {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String processId;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private CompanyDetails companyDetails;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private ContactDetails contactDetails;

    @OneToMany(mappedBy = "companyVehicleInsurance",
            cascade = CascadeType.ALL,fetch = FetchType.EAGER
    )
    @JsonManagedReference("companyVehicleInsurance_vehicles")
    private Set<CompanyInsuranceVehicleDetails> vehicles;

    @OneToOne(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    private CompanyInsuranceQuestions insuranceQuestions;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("company_companyVehicleInsuranceClients")
    private Company company;


}
