package com.kaributechs.insurehubdbengine.models.new_business.household_insurance;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class HouseholdInsuranceExtendedFamilyDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String lastName;
    private String firstName;
    private String salutation;
    private String nationalId;
    private Date dateOfBirth;
    private String emailAddress;
    private String phoneNumber;
    private String gender;
    private String identificationType;
    private String relationshipToBeneficiary;
    private String maritalStatus;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("householdInsurance_extendedFamily")
    private HouseholdInsurance householdInsurance;

}
