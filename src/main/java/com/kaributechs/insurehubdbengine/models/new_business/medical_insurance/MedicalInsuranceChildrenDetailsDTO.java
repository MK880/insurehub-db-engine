package com.kaributechs.insurehubdbengine.models.new_business.medical_insurance;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
public class MedicalInsuranceChildrenDetailsDTO {

    private String lastName;
    private String firstName;
    private String salutation;
    private String nationalId;
    private Date dateOfBirth;
    private String emailAddress;
    private String phoneNumber;
    private String gender;
    private String identificationType;
    private String relationshipToMember;
    private String funeralCover;


}
