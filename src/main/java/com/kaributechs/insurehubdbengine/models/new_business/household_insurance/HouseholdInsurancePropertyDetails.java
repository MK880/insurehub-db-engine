package com.kaributechs.insurehubdbengine.models.new_business.household_insurance;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class HouseholdInsurancePropertyDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private boolean isOccuppiedByFamilyOnly;
    private String occupationDescription;
    private String propertyUse;
    private String isLetToAFamily;
    private String numberOfTenants;
    private String isBuiltWithBricks;
    private String materialsUsedToBuild;
    private String isRoofedWithSlateOrTiles;
    private String materialsUsedToRoof;
    private String propertyHasPreservationOrder;
    private String preservationOrderDescription;
    private String propertyHasUniqueFeatures;
    private String uniqueFeatures;
    private String yearHouseWasBuilt;
    private String areaOfProperty;
    private String areaOfPropertyDetails;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private HouseholdInsurancePropertyAddress propertyAddress;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private HouseholdInsuranceProtectionDetails protectionDetails;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("householdInsurance_properties")
    private HouseholdInsurance householdInsurance;


}
