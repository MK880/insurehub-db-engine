package com.kaributechs.insurehubdbengine.models.new_business;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.InsurehubInsurance;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CompanyInsurance {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String key;

    private BigDecimal principalMemberPremium;
    private BigDecimal childBeneficiaryPremium;
    private BigDecimal adultBeneficiaryPremium;
    private BigDecimal seniorBeneficiaryPremium;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("company_insurances")
    private Company company;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private InsurehubInsurance insurehubInsurance;

}
