package com.kaributechs.insurehubdbengine.models.new_business.personal_vehicle_insurance;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
public class PersonalVehicleInsuranceVehicleDetailsDTO {

    private String vehicleRegistrationNumber;
    private String clientLicenseNumber;
    private String dateWhenLicenseWasObtained;
    private String vehicleMake;
    private String vehicleModel;
    private String vehicleYear;
    private String vehicleMileage;
    private String vehicleWorth;
    private String numberOfSeats;
    private String vehicleHasAlarmSystem;
    private String residentHasNightParking;
    private String numberOfDrivers;

    private PersonalInsuranceSecondaryDriverDetailsDTO secondaryDriverDetails;

}
