package com.kaributechs.insurehubdbengine.repositories;

import com.kaributechs.insurehubdbengine.models.dealer.DealerService;
import com.kaributechs.insurehubdbengine.models.dealer.Service;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DealerServiceRepository extends JpaRepository<DealerService,Long> {
    Optional<DealerService> findServiceByName(String name);
    List<DealerService> findDealerServicesByName(String name);
    List<DealerService> findDealerServicesByService(Service service);
}
