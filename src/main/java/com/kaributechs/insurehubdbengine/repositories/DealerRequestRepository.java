package com.kaributechs.insurehubdbengine.repositories;

import com.kaributechs.insurehubdbengine.models.dealer.DealerRequest;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DealerRequestRepository extends JpaRepository<DealerRequest,Long> {
    Optional<DealerRequest> findDealerRequestByProcessId(String processId);
}
