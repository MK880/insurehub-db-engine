package com.kaributechs.insurehubdbengine.repositories;

import com.kaributechs.insurehubdbengine.models.dealer.Quotation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuotationRepository extends JpaRepository<Quotation,Long> {
}
