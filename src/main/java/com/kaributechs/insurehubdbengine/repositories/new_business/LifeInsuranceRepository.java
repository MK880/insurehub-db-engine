package com.kaributechs.insurehubdbengine.repositories.new_business;

import com.kaributechs.insurehubdbengine.models.new_business.life_insurance.LifeInsurance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LifeInsuranceRepository extends JpaRepository<LifeInsurance,Long> {
    Optional<LifeInsurance> findLifeInsuranceByProcessId(String processId);
}
