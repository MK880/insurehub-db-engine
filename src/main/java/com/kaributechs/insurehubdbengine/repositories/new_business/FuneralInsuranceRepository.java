package com.kaributechs.insurehubdbengine.repositories.new_business;

import com.kaributechs.insurehubdbengine.models.new_business.funeral_insurance.FuneralInsurance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface FuneralInsuranceRepository extends JpaRepository<FuneralInsurance,Long> {
    Optional<FuneralInsurance> findFuneralInsuranceByProcessId(String processId);
}
