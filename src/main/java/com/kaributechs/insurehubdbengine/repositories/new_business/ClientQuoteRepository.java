package com.kaributechs.insurehubdbengine.repositories.new_business;

import com.kaributechs.insurehubdbengine.models.insurance_quotation.NewBusinessClientQuote;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientQuoteRepository extends JpaRepository<NewBusinessClientQuote,Long> {
}
