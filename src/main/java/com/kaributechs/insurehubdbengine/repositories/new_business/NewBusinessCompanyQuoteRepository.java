package com.kaributechs.insurehubdbengine.repositories.new_business;

import com.kaributechs.insurehubdbengine.models.insurance_quotation.NewBusinessCompanyQuote;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NewBusinessCompanyQuoteRepository extends JpaRepository<NewBusinessCompanyQuote,Long> {
}
