package com.kaributechs.insurehubdbengine.repositories.new_business;

import com.kaributechs.insurehubdbengine.models.new_business.medical_insurance.MedicalInsurance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MedicalInsuranceRepository extends JpaRepository<MedicalInsurance,Long> {
    Optional<MedicalInsurance> findMedicalInsuranceByProcessId(String processId);
}
