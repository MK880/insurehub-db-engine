package com.kaributechs.insurehubdbengine.repositories.new_business;

import com.kaributechs.insurehubdbengine.models.new_business.company_vehicle_insurance.CompanyInsuranceQuestions;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyInsuranceQuestionsRepository extends JpaRepository<CompanyInsuranceQuestions,Long> {
}
