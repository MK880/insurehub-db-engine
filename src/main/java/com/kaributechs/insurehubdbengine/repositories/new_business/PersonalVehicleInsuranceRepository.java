package com.kaributechs.insurehubdbengine.repositories.new_business;

import com.kaributechs.insurehubdbengine.models.new_business.personal_vehicle_insurance.PersonalVehicleInsurance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PersonalVehicleInsuranceRepository extends JpaRepository<PersonalVehicleInsurance,Long> {
    Optional<PersonalVehicleInsurance> findPersonalVehicleInsuranceByProcessId(String processId);
}
