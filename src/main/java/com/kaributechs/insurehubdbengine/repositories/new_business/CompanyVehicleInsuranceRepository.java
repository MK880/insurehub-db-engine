package com.kaributechs.insurehubdbengine.repositories.new_business;

import com.kaributechs.insurehubdbengine.models.new_business.company_vehicle_insurance.CompanyVehicleInsurance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CompanyVehicleInsuranceRepository extends JpaRepository<CompanyVehicleInsurance,Long> {
    Optional<CompanyVehicleInsurance> findCompanyVehicleInsuranceByProcessId(String processId);
}
