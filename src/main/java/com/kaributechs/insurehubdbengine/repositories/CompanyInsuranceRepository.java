package com.kaributechs.insurehubdbengine.repositories;

import com.kaributechs.insurehubdbengine.models.InsurehubInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.CompanyInsurance;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CompanyInsuranceRepository extends JpaRepository<CompanyInsurance,Long> {
    Optional<CompanyInsurance> findByKey(String key);
    List<CompanyInsurance> findAllByInsurehubInsurance(InsurehubInsurance insurance, Sort sort);
}
