package com.kaributechs.insurehubdbengine.repositories;

import com.kaributechs.insurehubdbengine.models.StaticAttachment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StaticAttachmentRepository extends JpaRepository<StaticAttachment,Long> {
}
