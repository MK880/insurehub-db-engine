package com.kaributechs.insurehubdbengine.repositories;

import com.kaributechs.insurehubdbengine.models.dealer.ServiceDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceDetailsRepositories extends JpaRepository<ServiceDetails,Long> {
}
