package com.kaributechs.insurehubdbengine.repositories;

import com.kaributechs.insurehubdbengine.models.dealer.Dealer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DealerRepository extends JpaRepository<Dealer,Long> {
    Optional<Dealer> findDealerByUsernameAndAvailable(String username, boolean available);
    Optional<Dealer> findDealerByUsername(String username);

}
