package com.kaributechs.insurehubdbengine.repositories;

import com.kaributechs.insurehubdbengine.models.TemplateBlueprint;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TemplateBlueprintRepository extends JpaRepository<TemplateBlueprint,Long> {
}
