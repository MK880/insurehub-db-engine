package com.kaributechs.insurehubdbengine.repositories;

import com.kaributechs.insurehubdbengine.models.InsurehubInsurance;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InsurehubInsuranceRepository extends JpaRepository<InsurehubInsurance,Long> {
}
