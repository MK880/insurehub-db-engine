package com.kaributechs.insurehubdbengine.repositories;

import com.kaributechs.insurehubdbengine.models.claims.Claim;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ClaimRepository extends JpaRepository<Claim,Long> {


    Optional<Claim> findByIdAndDeleted(String id, boolean deleted);

    Optional<Claim> findByProcessInstanceIdAndDeleted(String processId, boolean deleted);


    List<Claim> findAllByDeleted(boolean deleted);

    List<Claim> findAllByUsernameAndDeleted(String username,boolean deleted);
}
