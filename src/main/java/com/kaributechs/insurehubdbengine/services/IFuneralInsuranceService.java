package com.kaributechs.insurehubdbengine.services;

import com.kaributechs.insurehubdbengine.models.new_business.funeral_insurance.FuneralInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.funeral_insurance.FuneralInsuranceDTO;

public interface IFuneralInsuranceService {
    public FuneralInsurance save(FuneralInsuranceDTO funeralInsuranceDTO);
    FuneralInsurance getInsuranceByProcessId(String processId);
}
