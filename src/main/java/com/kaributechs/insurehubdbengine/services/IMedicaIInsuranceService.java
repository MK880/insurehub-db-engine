package com.kaributechs.insurehubdbengine.services;

import com.kaributechs.insurehubdbengine.models.new_business.medical_insurance.MedicalInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.medical_insurance.MedicalInsuranceDTO;

public interface IMedicaIInsuranceService {
    MedicalInsurance save(MedicalInsuranceDTO medicalInsurance);

    MedicalInsurance getInsuranceByProcessId(String processId);
}
