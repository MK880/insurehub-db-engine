package com.kaributechs.insurehubdbengine.services;

import com.kaributechs.insurehubdbengine.models.Template;
import com.kaributechs.insurehubdbengine.models.dtos.EditTemplateDTO;


public interface ITemplatesService {


    Template editTemplateImp(EditTemplateDTO editTemplateDTO);

    Template getTemplateByIdImp(Long id);

//    void sendTemplateEmailImp(TemplateEmailDTO templateEmailDTO) throws MessagingException, IOException, DocumentException;
}
