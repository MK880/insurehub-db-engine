package com.kaributechs.insurehubdbengine.services;

import com.kaributechs.insurehubdbengine.models.new_business.company_vehicle_insurance.CompanyVehicleInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.company_vehicle_insurance.CompanyVehicleInsuranceDTO;

public interface ICompanyVehicleInsuranceService {
    CompanyVehicleInsurance apply(CompanyVehicleInsuranceDTO companyVehicleInsurance);

    CompanyVehicleInsurance getInsuranceByProcessId(String processId);
}
