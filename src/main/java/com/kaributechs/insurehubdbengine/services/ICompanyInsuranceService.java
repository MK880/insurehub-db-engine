package com.kaributechs.insurehubdbengine.services;

import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.new_business.CompanyInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.UpdateCompanyInsuranceDTO;
import com.kaributechs.insurehubdbengine.models.new_business.personal_vehicle_insurance.AddCompanyInsuranceDTO;

import java.util.Optional;

public interface ICompanyInsuranceService {
    Company addCompanyInsurance(AddCompanyInsuranceDTO addCompanyInsuranceDTO);

    CompanyInsurance getCompanyInsuranceById(Long id);

    CompanyInsurance updateCompanyInsurance(UpdateCompanyInsuranceDTO updateCompanyInsuranceDTO);
}


