package com.kaributechs.insurehubdbengine.services;

import com.kaributechs.insurehubdbengine.models.insurance_quotation.RequestCompanyQuoteDTO;
import com.kaributechs.insurehubdbengine.models.insurance_quotation.RequestQuoteDTO;
import com.kaributechs.insurehubdbengine.models.insurance_quotation.NewBusinessClientQuotation;

public interface IQuotationService {
    NewBusinessClientQuotation getQuote(RequestQuoteDTO insuranceId);

    NewBusinessClientQuotation getCompanyQuotation(RequestCompanyQuoteDTO requestCompanyQuoteDTO);
}
