package com.kaributechs.insurehubdbengine.services;

import com.kaributechs.insurehubdbengine.models.dealer.Service;
import com.kaributechs.insurehubdbengine.models.dtos.ServiceDTO;

import java.util.List;

public interface IServicesService {
    Service addService(ServiceDTO serviceDTO);

    Service updateService(ServiceDTO serviceDTO);

    Service getServiceById(Long id);

    void deleteServiceById(Long id);

    List<Service> getAllServices();
}
