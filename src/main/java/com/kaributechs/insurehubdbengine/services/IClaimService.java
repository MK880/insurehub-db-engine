package com.kaributechs.insurehubdbengine.services;


import com.kaributechs.insurehubdbengine.models.claims.Claim;
import com.kaributechs.insurehubdbengine.models.dtos.ClaimDTO;
import com.kaributechs.insurehubdbengine.models.dtos.ResponseDTO;

import java.util.List;

public interface IClaimService {

    Claim addClaimImp(ClaimDTO claimDTO);

    Claim getClaimByIdImp(String id);

    List<Claim> getAllClaimsImp();

    ResponseDTO deleteClaimImp(String id);

    Claim updateClaimImp(Claim claim);

    List<Claim> getClaimByUsername(String username);

    Claim getClaimByProcessInstanceId(String processId);
}
