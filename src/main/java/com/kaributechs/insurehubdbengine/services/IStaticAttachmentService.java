package com.kaributechs.insurehubdbengine.services;

import com.kaributechs.insurehubdbengine.models.StaticAttachment;
import com.kaributechs.insurehubdbengine.models.dtos.AddStaticAttachmentDTO;
import com.kaributechs.insurehubdbengine.models.dtos.UpdateStaticAttachmentDTO;

import java.io.IOException;

public interface IStaticAttachmentService {
    StaticAttachment addStaticAttachmentImp(AddStaticAttachmentDTO addStaticAttachmentDTO) throws IOException;

    StaticAttachment getStaticAttachmentByIdImp(Long id);

    StaticAttachment updateStaticAttachment(UpdateStaticAttachmentDTO updateStaticAttachmentDTO) throws IOException;
}
