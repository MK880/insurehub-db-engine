package com.kaributechs.insurehubdbengine.services;

import com.kaributechs.insurehubdbengine.models.TemplateBlueprint;

import java.util.List;

public interface ITemplateBlueprintService {
    TemplateBlueprint addTemplateBlueprint(TemplateBlueprint templateBlueprint);

    List<TemplateBlueprint> getAllTemplateBlueprintsImp();

    TemplateBlueprint editTemplateBlueprintImp(TemplateBlueprint templateBlueprint);

    TemplateBlueprint getTemplateBlueprintByIdImp(Long id);
}
