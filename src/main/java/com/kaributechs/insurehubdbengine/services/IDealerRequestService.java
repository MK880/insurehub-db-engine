package com.kaributechs.insurehubdbengine.services;

import com.kaributechs.insurehubdbengine.models.dealer.DealerRequest;
import com.kaributechs.insurehubdbengine.models.dealer.DealerRequestQuotationDTO;
import com.kaributechs.insurehubdbengine.models.dealer.DealerService;
import com.kaributechs.insurehubdbengine.models.dealer.Service;

import java.util.List;

public interface IDealerRequestService {
    DealerRequest addDealerRequestImp(DealerRequestQuotationDTO dealerRequestQuotationDTO);

    DealerRequest getDealerRequestById(Long id);

    DealerRequest getDealerRequestByProcessInstanceId(String processId);

    List<DealerRequest> getDealerRequestByUsername(String username);

    List<DealerService> getDealerRequestsByService(Service service);
}
