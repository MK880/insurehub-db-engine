package com.kaributechs.insurehubdbengine.services;

import com.kaributechs.insurehubdbengine.models.new_business.household_insurance.HouseholdInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.household_insurance.HouseholdInsuranceDTO;

public interface IHouseholdInsuranceService {
    HouseholdInsurance save(HouseholdInsuranceDTO householdInsuranceDTO);
    HouseholdInsurance getInsuranceByProcessId(String processId);
}
