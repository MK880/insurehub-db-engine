package com.kaributechs.insurehubdbengine.services.implementation;

import com.kaributechs.insurehubdbengine.exceptions.AlreadyExists;
import com.kaributechs.insurehubdbengine.exceptions.ResourceNotFound;
import com.kaributechs.insurehubdbengine.models.dtos.ServiceDTO;
import com.kaributechs.insurehubdbengine.repositories.ServicesRepository;
import com.kaributechs.insurehubdbengine.services.IServicesService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServicesServiceImp implements IServicesService {
    private final ServicesRepository servicesRepository;

    public ServicesServiceImp(ServicesRepository servicesRepository) {
        this.servicesRepository = servicesRepository;
    }

    @Override
    public com.kaributechs.insurehubdbengine.models.dealer.Service addService(ServiceDTO serviceDTO) {
        if (servicesRepository.findServiceByName(serviceDTO.getName()).isPresent()) {
            throw new AlreadyExists("Service","name",serviceDTO.getName());
        } else {
            com.kaributechs.insurehubdbengine.models.dealer.Service service = new com.kaributechs.insurehubdbengine.models.dealer.Service();
            service.setName(serviceDTO.getName());
            return servicesRepository.save(service);
        }
    }

    @Override
    public com.kaributechs.insurehubdbengine.models.dealer.Service getServiceById(Long id) {
        return servicesRepository.findById(id).orElseThrow(()->new ResourceNotFound("Service","id",id));
    }

    @Override
    public com.kaributechs.insurehubdbengine.models.dealer.Service updateService(ServiceDTO serviceDTO) {
        if (!servicesRepository.findById(serviceDTO.getId()).isPresent()) {
            throw new ResourceNotFound("Service","id",serviceDTO.getId());
        } else {
            com.kaributechs.insurehubdbengine.models.dealer.Service service = getServiceById(serviceDTO.getId());
            service.setName(serviceDTO.getName());
            return servicesRepository.save(service);
        }
    }

    @Override
    public void deleteServiceById(Long id) {
        if (!servicesRepository.findById(id).isPresent()) {
            throw new ResourceNotFound("Service","id",id);
        } else {

            servicesRepository.delete(getServiceById(id));
        }
    }

    @Override
    public List<com.kaributechs.insurehubdbengine.models.dealer.Service> getAllServices() {
        return servicesRepository.findAll();
    }
}
