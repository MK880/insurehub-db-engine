package com.kaributechs.insurehubdbengine.services.implementation;

import com.kaributechs.insurehubdbengine.exceptions.ResourceNotFound;
import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.new_business.ContactDetails;
import com.kaributechs.insurehubdbengine.models.new_business.SpouseDetails;
import com.kaributechs.insurehubdbengine.models.new_business.life_insurance.*;
import com.kaributechs.insurehubdbengine.repositories.new_business.LifeInsuranceRepository;
import com.kaributechs.insurehubdbengine.services.ICompanyService;
import com.kaributechs.insurehubdbengine.services.ILifeInsuranceService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class LifeInsuranceServiceImp implements ILifeInsuranceService {
    private final LifeInsuranceRepository lifeInsuranceRepository;
    private final ICompanyService companyService;

    public LifeInsuranceServiceImp(LifeInsuranceRepository lifeInsuranceRepository, ICompanyService companyService) {
        this.lifeInsuranceRepository = lifeInsuranceRepository;
        this.companyService = companyService;
    }

    @Override
    public LifeInsurance save(LifeInsuranceDTO lifeInsuranceDTO) {
        LifeInsurance lifeInsurance = new LifeInsurance();
        BeanUtils.copyProperties(lifeInsuranceDTO, lifeInsurance);

        LifeInsuranceMemberDetails membersDetails = new LifeInsuranceMemberDetails();
        BeanUtils.copyProperties(lifeInsuranceDTO.getMemberDetails(), membersDetails);
        lifeInsurance.setMemberDetails(membersDetails);

        ContactDetails contactDetails = new ContactDetails();
        BeanUtils.copyProperties(lifeInsuranceDTO.getContactDetails(), contactDetails);
        lifeInsurance.setContactDetails(contactDetails);

        OtherInsuranceDetails otherInsuranceDetails = new OtherInsuranceDetails();
        BeanUtils.copyProperties(lifeInsuranceDTO.getOtherInsuranceDetails(), otherInsuranceDetails);
        lifeInsurance.setOtherInsuranceDetails(otherInsuranceDetails);

        Set<LifeInsuranceExtendedFamilyDetails> extendedFamily = new HashSet<>();
        if (lifeInsuranceDTO.getExtendedFamily() != null) {
            lifeInsuranceDTO.getExtendedFamily().forEach(extendedFamilyMember -> {
                LifeInsuranceExtendedFamilyDetails member = new LifeInsuranceExtendedFamilyDetails();
                BeanUtils.copyProperties(extendedFamilyMember, member);
                member.setLifeInsurance(lifeInsurance);
                extendedFamily.add(member);
            });
            lifeInsurance.setExtendedFamily(extendedFamily);
        }

        Set<LifeInsuranceBeneficiaryDetails> beneficiaries = new HashSet<>();
        if (lifeInsuranceDTO.getBeneficiaries() != null) {
            lifeInsuranceDTO.getBeneficiaries().forEach(beneficiary -> {
                        LifeInsuranceBeneficiaryDetails beneficiaryDetails = new LifeInsuranceBeneficiaryDetails();
                        BeanUtils.copyProperties(beneficiary, beneficiaryDetails);
                        beneficiaryDetails.setLifeInsurance(lifeInsurance);
                        beneficiaries.add(beneficiaryDetails);
                    });
            lifeInsurance.setBeneficiaries(beneficiaries);
        }

        if (lifeInsuranceDTO.getSpouse()!=null) {
            SpouseDetails spouseDetails = new SpouseDetails();
            BeanUtils.copyProperties(lifeInsuranceDTO.getSpouse(), spouseDetails);
            lifeInsurance.setSpouse(spouseDetails);
        }

        LifeInsuranceQuestions lifeInsuranceQuestions=new LifeInsuranceQuestions();
        BeanUtils.copyProperties(lifeInsuranceDTO.getInsuranceQuestions(),lifeInsuranceQuestions);
        lifeInsurance.setInsuranceQuestions(lifeInsuranceQuestions);

        Company company=companyService.getCompanyByIdImp(lifeInsuranceDTO.getCompanyId());
        lifeInsurance.setCompany(company);

        if (company.getLifeInsuranceClients() !=null){
            company.getLifeInsuranceClients().add(lifeInsurance);
        }else{
            Set<LifeInsurance> medicalInsurances= new HashSet<>();
            medicalInsurances.add(lifeInsurance);
            company.setLifeInsuranceClients(medicalInsurances);
        }
        return lifeInsuranceRepository.save(lifeInsurance);
    }

    @Override
    public LifeInsurance getInsuranceByProcessId(String processId) {
        return lifeInsuranceRepository.findLifeInsuranceByProcessId(processId).orElseThrow(() -> new ResourceNotFound("Life Insurance", "processId", processId));
    }
}
