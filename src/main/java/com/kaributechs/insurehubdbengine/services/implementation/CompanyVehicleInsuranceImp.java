package com.kaributechs.insurehubdbengine.services.implementation;

import com.kaributechs.insurehubdbengine.exceptions.ResourceNotFound;
import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.new_business.ContactDetails;
import com.kaributechs.insurehubdbengine.models.new_business.company_vehicle_insurance.CompanyInsuranceVehicleDetails;
import com.kaributechs.insurehubdbengine.models.new_business.VehicleUserDetails;
import com.kaributechs.insurehubdbengine.models.new_business.company_vehicle_insurance.*;
import com.kaributechs.insurehubdbengine.models.new_business.medical_insurance.MedicalInsurance;
import com.kaributechs.insurehubdbengine.repositories.new_business.CompanyVehicleInsuranceRepository;
import com.kaributechs.insurehubdbengine.services.ICompanyService;
import com.kaributechs.insurehubdbengine.services.ICompanyVehicleInsuranceService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class CompanyVehicleInsuranceImp implements ICompanyVehicleInsuranceService {
    private final CompanyVehicleInsuranceRepository companyVehicleInsuranceRepository;
    private final ICompanyService companyService;

    public CompanyVehicleInsuranceImp(CompanyVehicleInsuranceRepository companyVehicleInsuranceRepository, ICompanyService companyService) {
        this.companyVehicleInsuranceRepository = companyVehicleInsuranceRepository;
        this.companyService = companyService;
    }

    @Override
    public CompanyVehicleInsurance apply(CompanyVehicleInsuranceDTO companyVehicleInsuranceDTO) {
        CompanyVehicleInsurance companyVehicleInsurance = new CompanyVehicleInsurance();
        BeanUtils.copyProperties(companyVehicleInsuranceDTO, companyVehicleInsurance);

        CompanyDetails companyDetails = new CompanyDetails();
        BeanUtils.copyProperties(companyVehicleInsuranceDTO.getCompanyDetails(), companyDetails);
        companyVehicleInsurance.setCompanyDetails(companyDetails);

        ContactDetails contactDetails = new ContactDetails();
        BeanUtils.copyProperties(companyVehicleInsuranceDTO.getContactDetails(), contactDetails);
        companyVehicleInsurance.setContactDetails(contactDetails);

        CompanyInsuranceQuestions companyInsuranceQuestions = new CompanyInsuranceQuestions();
        BeanUtils.copyProperties(companyVehicleInsuranceDTO.getInsuranceQuestions(), companyInsuranceQuestions);
        companyVehicleInsurance.setInsuranceQuestions(companyInsuranceQuestions);

        Set<CompanyInsuranceVehicleDetails> companyInsuranceVehicleDetails = new HashSet<>();
        if (companyVehicleInsuranceDTO.getVehicles() != null) {
            companyVehicleInsuranceDTO.getVehicles().forEach(vehicle -> {
                CompanyInsuranceVehicleDetails vehicleDetails = new CompanyInsuranceVehicleDetails();
                BeanUtils.copyProperties(vehicle, vehicleDetails);

                VehicleUserDetails vehicleUserDetails = new VehicleUserDetails();
                BeanUtils.copyProperties(vehicle.getVehicleUser(), vehicleUserDetails);
                vehicleDetails.setVehicleUser(vehicleUserDetails);
                vehicleDetails.setCompanyVehicleInsurance(companyVehicleInsurance);

                List<OtherVehicleUserDetails> otherVehicleUserDetailsList = new ArrayList<>();
                if (vehicle.getOtherVehicleUsers() != null) {
                    vehicle.getOtherVehicleUsers().forEach(otherUser -> {
                        OtherVehicleUserDetails otherVehicleUser = new OtherVehicleUserDetails();
                        BeanUtils.copyProperties(otherUser, otherVehicleUser);
                        otherVehicleUser.setVehicleDetails(vehicleDetails);
                        otherVehicleUserDetailsList.add(otherVehicleUser);
                    });
                    vehicleDetails.setOtherVehicleUsers(otherVehicleUserDetailsList);
                }
                companyInsuranceVehicleDetails.add(vehicleDetails);
            });
            companyVehicleInsurance.setVehicles(companyInsuranceVehicleDetails);

        }

        Company company=companyService.getCompanyByIdImp(companyVehicleInsuranceDTO.getCompanyId());
        companyVehicleInsurance.setCompany(company);

        if (company.getCompanyVehicleInsuranceClients() !=null){
            company.getCompanyVehicleInsuranceClients().add(companyVehicleInsurance);
        }else{
            Set<CompanyVehicleInsurance> medicalInsurances= new HashSet<>();
            medicalInsurances.add(companyVehicleInsurance);
            company.setCompanyVehicleInsuranceClients(medicalInsurances);
        }


        return companyVehicleInsuranceRepository.save(companyVehicleInsurance);
    }

    @Override
    public CompanyVehicleInsurance getInsuranceByProcessId(String processId) {
        return companyVehicleInsuranceRepository.findCompanyVehicleInsuranceByProcessId(processId).orElseThrow(() -> new ResourceNotFound("Company Vehicle Insurance", "processId", processId));
    }
}
