package com.kaributechs.insurehubdbengine.services.implementation;

import com.kaributechs.insurehubdbengine.models.dealer.RegisterDealerDTO;
import com.kaributechs.insurehubdbengine.exceptions.AlreadyExists;
import com.kaributechs.insurehubdbengine.exceptions.ResourceNotFound;
import com.kaributechs.insurehubdbengine.exceptions.ServiceNotFoundException;
import com.kaributechs.insurehubdbengine.models.dealer.Dealer;
import com.kaributechs.insurehubdbengine.models.dealer.AddDealerServiceDTO;
import com.kaributechs.insurehubdbengine.models.dealer.DealerService;
import com.kaributechs.insurehubdbengine.repositories.DealerRepository;
import com.kaributechs.insurehubdbengine.repositories.DealerServiceRepository;
import com.kaributechs.insurehubdbengine.repositories.ServicesRepository;
import com.kaributechs.insurehubdbengine.services.IDealerService;
import com.kaributechs.insurehubdbengine.utilities.Utilities;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DealerServiceServiceImp implements IDealerService {
    private final DealerRepository dealerRepository;
    private final DealerServiceRepository dealerServiceRepository;
    private final ServicesRepository servicesRepository;
    private final Utilities utilities;

    public DealerServiceServiceImp(DealerRepository dealerRepository, DealerServiceRepository dealerServiceRepository, ServicesRepository servicesRepository, Utilities utilities) {
        this.dealerRepository = dealerRepository;
        this.dealerServiceRepository = dealerServiceRepository;
        this.servicesRepository = servicesRepository;
        this.utilities = utilities;
    }

    @Override
    public Dealer registerDealer(RegisterDealerDTO registerDealerDTO) {


        if (dealerRepository.findDealerByUsername(registerDealerDTO.getUsername()).isPresent()) {
            throw new AlreadyExists("Dealer","Username",registerDealerDTO.getUsername());
        } else {
            Dealer dealer = new Dealer();
            BeanUtils.copyProperties(registerDealerDTO,dealer);
            return dealerRepository.save(dealer);
        }
    }

    @Override
    public Dealer addDealerService(AddDealerServiceDTO addDealerServiceDTO) {
        com.kaributechs.insurehubdbengine.models.dealer.Service service=servicesRepository.findById(addDealerServiceDTO.getServiceId()).orElseThrow(()->new ServiceNotFoundException(addDealerServiceDTO.getServiceId()));
        log.info("found service {}",service.getName());
        Dealer dealer = getDealerByUsername(utilities.getUsername());
        log.info("found dealer {}",dealer.getUsername());
        dealer.getDealerServices().forEach(dealerService -> {
            if (dealerService.getName().equals(service.getName())){
                throw new AlreadyExists("Dealer Service","name",service.getName());
            }
        });
        DealerService newDealerService =new DealerService();
        newDealerService.setName(service.getName());
        newDealerService.setService(service);
        newDealerService.setPrice(addDealerServiceDTO.getPrice());
        newDealerService.setDealer(dealer);
        dealer.getDealerServices().add(newDealerService);
        return dealerRepository.save(dealer);
    }

    @Override
    public Dealer getDealerByUsername(String username) {
        return dealerRepository.findDealerByUsername(username).orElseThrow(() -> new ResourceNotFound("Dealer", username, username));
    }




}
