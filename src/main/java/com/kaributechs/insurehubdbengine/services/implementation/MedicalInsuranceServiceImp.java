package com.kaributechs.insurehubdbengine.services.implementation;

import com.kaributechs.insurehubdbengine.exceptions.ResourceNotFound;
import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.new_business.ContactDetails;
import com.kaributechs.insurehubdbengine.models.new_business.SpouseDetails;
import com.kaributechs.insurehubdbengine.models.new_business.medical_insurance.*;
import com.kaributechs.insurehubdbengine.models.new_business.personal_vehicle_insurance.PersonalVehicleInsurance;
import com.kaributechs.insurehubdbengine.repositories.new_business.MedicalInsuranceRepository;
import com.kaributechs.insurehubdbengine.services.ICompanyService;
import com.kaributechs.insurehubdbengine.services.IMedicaIInsuranceService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class MedicalInsuranceServiceImp implements IMedicaIInsuranceService {
    private final MedicalInsuranceRepository medicalInsuranceRepository;
    private final ICompanyService companyService;


    public MedicalInsuranceServiceImp(MedicalInsuranceRepository medicalInsuranceRepository, ICompanyService companyService) {
        this.medicalInsuranceRepository = medicalInsuranceRepository;
        this.companyService = companyService;
    }

    @Override
    public MedicalInsurance save(MedicalInsuranceDTO medicalInsuranceDTO) {
        MedicalInsurance medicalInsurance = new MedicalInsurance();
        BeanUtils.copyProperties(medicalInsuranceDTO, medicalInsurance);

        MedicalInsuranceMemberDetails memberDetails = new MedicalInsuranceMemberDetails();
        BeanUtils.copyProperties(medicalInsuranceDTO.getMemberDetails(), memberDetails);
        medicalInsurance.setMemberDetails(memberDetails);

        ContactDetails contactDetails = new ContactDetails();
        BeanUtils.copyProperties(medicalInsuranceDTO.getContactDetails(), contactDetails);
        medicalInsurance.setContactDetails(contactDetails);


        SpouseDetails spouseDetails = new SpouseDetails();
        if (medicalInsuranceDTO.getSpouse() != null) {
            BeanUtils.copyProperties(medicalInsuranceDTO.getSpouse(), spouseDetails);
            medicalInsurance.setSpouse(spouseDetails);
        }

        Set<MedicalInsuranceChildrenDetails> medicalInsuranceChildrenDetailsList = new HashSet<>();
        if (medicalInsuranceDTO.getChildren() != null) {
            medicalInsuranceDTO.getChildren().forEach(child -> {
                MedicalInsuranceChildrenDetails childDetails = new MedicalInsuranceChildrenDetails();
                BeanUtils.copyProperties(child, childDetails);
                childDetails.setMedicalInsurance(medicalInsurance);
                medicalInsuranceChildrenDetailsList.add(childDetails);
            });
            medicalInsurance.setChildren(medicalInsuranceChildrenDetailsList);

        }

        Set<MedicalInsuranceExtendedFamilyDetails> medicalInsuranceExtendedFamilyDetailsList = new HashSet<>();
        if (medicalInsuranceDTO.getExtendedFamily() != null) {
            medicalInsuranceDTO.getExtendedFamily().forEach(member -> {
                MedicalInsuranceExtendedFamilyDetails medicalInsuranceExtendedFamilyDetails = new MedicalInsuranceExtendedFamilyDetails();
                BeanUtils.copyProperties(member, medicalInsuranceExtendedFamilyDetails);
                medicalInsuranceExtendedFamilyDetails.setMedicalInsurance(medicalInsurance);
                medicalInsuranceExtendedFamilyDetailsList.add(medicalInsuranceExtendedFamilyDetails);
            });
            medicalInsurance.setExtendedFamily(medicalInsuranceExtendedFamilyDetailsList);

        }

        Set<MedicalInsuranceBeneficiaryDetails> beneficiaryDetailsList = new HashSet<>();
        if (medicalInsuranceDTO.getBeneficiaries() != null) {
            medicalInsuranceDTO.getBeneficiaries().forEach(beneficiary -> {
                MedicalInsuranceBeneficiaryDetails medicalInsuranceBeneficiaryDetails = new MedicalInsuranceBeneficiaryDetails();
                BeanUtils.copyProperties(beneficiary, medicalInsuranceBeneficiaryDetails);
                medicalInsuranceBeneficiaryDetails.setMedicalInsurance(medicalInsurance);
                beneficiaryDetailsList.add(medicalInsuranceBeneficiaryDetails);
            });
            medicalInsurance.setBeneficiaries(beneficiaryDetailsList);

        }

        MedicalInsuranceQuestions medicalInsuranceQuestions = new MedicalInsuranceQuestions();
        BeanUtils.copyProperties(medicalInsuranceDTO.getInsuranceQuestions(), medicalInsuranceQuestions);
        medicalInsurance.setInsuranceQuestions(medicalInsuranceQuestions);

        Company company=companyService.getCompanyByIdImp(medicalInsuranceDTO.getCompanyId());
        medicalInsurance.setCompany(company);

        if (company.getMedicalInsuranceClients() !=null){
            company.getMedicalInsuranceClients().add(medicalInsurance);
        }else{
            Set<MedicalInsurance> medicalInsurances= new HashSet<>();
            medicalInsurances.add(medicalInsurance);
            company.setMedicalInsuranceClients(medicalInsurances);
        }

        return medicalInsuranceRepository.save(medicalInsurance);
    }

    @Override
    public MedicalInsurance getInsuranceByProcessId(String processId) {
        return medicalInsuranceRepository.findMedicalInsuranceByProcessId(processId).orElseThrow(() -> new ResourceNotFound("Medical Insurance", "processId", processId));
    }
}
