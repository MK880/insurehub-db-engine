package com.kaributechs.insurehubdbengine.services.implementation;

import com.kaributechs.insurehubdbengine.exceptions.ResourceNotFound;
import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.new_business.ContactDetails;
import com.kaributechs.insurehubdbengine.models.new_business.SpouseDetails;
import com.kaributechs.insurehubdbengine.models.new_business.funeral_insurance.*;
import com.kaributechs.insurehubdbengine.repositories.new_business.FuneralInsuranceRepository;
import com.kaributechs.insurehubdbengine.services.ICompanyService;
import com.kaributechs.insurehubdbengine.services.IFuneralInsuranceService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class FuneralInsuranceServiceImp implements IFuneralInsuranceService {
    private final FuneralInsuranceRepository funeralInsuranceRepository;
    private final ICompanyService companyService;


    public FuneralInsuranceServiceImp(FuneralInsuranceRepository funeralInsuranceRepository, ICompanyService companyService) {
        this.funeralInsuranceRepository = funeralInsuranceRepository;
        this.companyService = companyService;
    }

    @Override
    public FuneralInsurance save(FuneralInsuranceDTO funeralInsuranceDTO) {
        FuneralInsurance funeralInsurance = new FuneralInsurance();
        BeanUtils.copyProperties(funeralInsuranceDTO,funeralInsurance);

        FuneralInsuranceMemberDetails memberDetails=new FuneralInsuranceMemberDetails();
        BeanUtils.copyProperties(funeralInsuranceDTO.getMemberDetails(),memberDetails);
        funeralInsurance.setMemberDetails(memberDetails);

        ContactDetails contactDetails=new ContactDetails();
        BeanUtils.copyProperties(funeralInsuranceDTO.getContactDetails(),contactDetails);
        funeralInsurance.setContactDetails(contactDetails);

        Set<FuneralInsuranceChildrenDetails> childrenSet=new HashSet<>();
        if (funeralInsuranceDTO.getChildren()!=null){
            funeralInsuranceDTO.getChildren().forEach(child->{
                FuneralInsuranceChildrenDetails childrenDetails=new FuneralInsuranceChildrenDetails();
                BeanUtils.copyProperties(child,childrenDetails);
                childrenDetails.setFuneralInsurance(funeralInsurance);
                childrenSet.add(childrenDetails);
            });
            funeralInsurance.setChildren(childrenSet);
        }

        Set<FuneralInsuranceExtendedFamilyDetails> extendedFamilyDetailsSet=new HashSet<>();
        if (funeralInsuranceDTO.getExtendedFamily()!=null){
            funeralInsuranceDTO.getExtendedFamily().forEach(member->{
                FuneralInsuranceExtendedFamilyDetails extendedFamilyDetails=new FuneralInsuranceExtendedFamilyDetails();
                BeanUtils.copyProperties(member,extendedFamilyDetails);
                extendedFamilyDetails.setFuneralInsurance(funeralInsurance);
                extendedFamilyDetailsSet.add(extendedFamilyDetails);
            });
            funeralInsurance.setExtendedFamily(extendedFamilyDetailsSet);
        }

        Set<FuneralInsuranceBeneficiaryDetails> beneficiaries = new HashSet<>();
        if (funeralInsuranceDTO.getBeneficiaries() != null) {
            funeralInsuranceDTO.getBeneficiaries().forEach(beneficiary -> {
                FuneralInsuranceBeneficiaryDetails beneficiaryDetails = new FuneralInsuranceBeneficiaryDetails();
                BeanUtils.copyProperties(beneficiary, beneficiaryDetails);
                beneficiaryDetails.setFuneralInsurance(funeralInsurance);
                beneficiaries.add(beneficiaryDetails);
            });
            funeralInsurance.setBeneficiaries(beneficiaries);
        }

        SpouseDetails spouseDetails = new SpouseDetails();
        if (funeralInsuranceDTO.getSpouse()!=null) {
            BeanUtils.copyProperties(funeralInsuranceDTO.getSpouse(), spouseDetails);
            funeralInsurance.setSpouse(spouseDetails);
        }

        FuneralInsuranceQuestions funeralInsuranceQuestions=new FuneralInsuranceQuestions();
        BeanUtils.copyProperties(funeralInsuranceDTO.getInsuranceQuestions(),funeralInsuranceQuestions);
        funeralInsurance.setInsuranceQuestions(funeralInsuranceQuestions);

        Company company=companyService.getCompanyByIdImp(funeralInsuranceDTO.getCompanyId());
        funeralInsurance.setCompany(company);

        if (company.getFuneralInsuranceClients() !=null){
            company.getFuneralInsuranceClients().add(funeralInsurance);
        }else{
            Set<FuneralInsurance> medicalInsurances= new HashSet<>();
            medicalInsurances.add(funeralInsurance);
            company.setFuneralInsuranceClients(medicalInsurances);
        }

        return funeralInsuranceRepository.save(funeralInsurance);
    }

    @Override
    public FuneralInsurance getInsuranceByProcessId(String processId) {
        return funeralInsuranceRepository.findFuneralInsuranceByProcessId(processId).orElseThrow(() -> new ResourceNotFound("Funeral Insurance", "processId", processId));
    }
}
