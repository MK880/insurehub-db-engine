package com.kaributechs.insurehubdbengine.services.implementation;

import com.kaributechs.insurehubdbengine.exceptions.GenericException;
import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.InsurehubInsurance;
import com.kaributechs.insurehubdbengine.models.insurance_quotation.*;
import com.kaributechs.insurehubdbengine.models.new_business.*;
import com.kaributechs.insurehubdbengine.repositories.CompanyInsuranceRepository;
import com.kaributechs.insurehubdbengine.repositories.new_business.NewBusinessClientQuotationRepository;
import com.kaributechs.insurehubdbengine.repositories.new_business.NewBusinessCompanyQuoteRepository;
import com.kaributechs.insurehubdbengine.services.ICompanyService;
import com.kaributechs.insurehubdbengine.services.IInsurehubInsuranceService;
import com.kaributechs.insurehubdbengine.services.IQuotationService;
import com.kaributechs.insurehubdbengine.utilities.Utilities;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service
@Slf4j
public class QuotationServiceImp implements IQuotationService {
    private final NewBusinessClientQuotationRepository clientQuotationRepository;
    private final IInsurehubInsuranceService insurehubInsuranceService;
    private final CompanyInsuranceRepository companyInsuranceRepository;
    private final NewBusinessCompanyQuoteRepository newBusinessCompanyQuoteRepository;
    private final Utilities utilities;
    private final ICompanyService companyService;

    public QuotationServiceImp(NewBusinessClientQuotationRepository clientQuotationRepository, IInsurehubInsuranceService insurehubInsuranceService, CompanyInsuranceRepository companyInsuranceRepository, NewBusinessCompanyQuoteRepository newBusinessCompanyQuoteRepository, Utilities utilities, ICompanyService companyService) {
        this.clientQuotationRepository = clientQuotationRepository;
        this.insurehubInsuranceService = insurehubInsuranceService;
        this.companyInsuranceRepository = companyInsuranceRepository;
        this.newBusinessCompanyQuoteRepository = newBusinessCompanyQuoteRepository;
        this.utilities = utilities;
        this.companyService = companyService;
    }

    @Override
    public NewBusinessClientQuotation getQuote(RequestQuoteDTO requestQuoteDTO) {
        InsurehubInsurance insurehubInsurance = insurehubInsuranceService.getInsuranceById(requestQuoteDTO.getInsuranceId());

        Sort sort = Sort.by(
                Sort.Order.asc("principalMemberPremium"));
        List<CompanyInsurance> insuranceList = companyInsuranceRepository.findAllByInsurehubInsurance(insurehubInsurance, sort);
        NewBusinessClientQuotation clientQuotation = new NewBusinessClientQuotation();
        clientQuotation.setInsurance(insurehubInsurance.getName());

        Set<NewBusinessClientQuote> clientQuotes = new HashSet<>();
        insuranceList.forEach(insurance -> {
            ClientQuotationCompanyDetails clientQuotationCompanyDetails = new ClientQuotationCompanyDetails();
            clientQuotationCompanyDetails.setCompany(insurance.getCompany().getName());
            clientQuotationCompanyDetails.setEmail(insurance.getCompany().getEmail());
            clientQuotationCompanyDetails.setMobileNumber(insurance.getCompany().getPhoneNumber());

            ClientQuotationPremiumDetails clientQuotationPremiumDetails = new ClientQuotationPremiumDetails();

            BeanUtils.copyProperties(insurance, clientQuotationPremiumDetails);
            clientQuotationPremiumDetails.setId(null);


            NewBusinessClientQuote clientQuote = new NewBusinessClientQuote();
            clientQuote.setCompanyDetails(clientQuotationCompanyDetails);
            clientQuote.setPremiumDetails(clientQuotationPremiumDetails);
            clientQuote.setClientQuotations(clientQuotation);

            clientQuotes.add(clientQuote);


            NewBusinessCompanyQuote companyQuote = new NewBusinessCompanyQuote();
            companyQuote.setCompany(insurance.getCompany());
            companyQuote.setEmail(requestQuoteDTO.getEmail());
            companyQuote.setMobileNumber(requestQuoteDTO.getMobileNumber());
            companyQuote.setInsurance(insurehubInsurance);
            newBusinessCompanyQuoteRepository.save(companyQuote);

        });
        clientQuotation.setQuotations(clientQuotes);

        if (requestQuoteDTO.isSendEmail() && requestQuoteDTO.getEmail() != null) {
            sendQuotationEmail(requestQuoteDTO.getEmail(), insurehubInsurance, clientQuotes);
        }

        return clientQuotationRepository.save(clientQuotation);
    }

    @Override
    public NewBusinessClientQuotation getCompanyQuotation(RequestCompanyQuoteDTO requestCompanyQuoteDTO) {
        InsurehubInsurance insurehubInsurance = insurehubInsuranceService.getInsuranceById(requestCompanyQuoteDTO.getInsuranceId());
        Company company = companyService.getCompanyByIdImp(requestCompanyQuoteDTO.getCompanyId());

        CompanyInsurance companyInsurance = null;
        for (CompanyInsurance insurance : company.getInsurances()) {
            if (insurance.getInsurehubInsurance().equals(insurehubInsurance)) {
                companyInsurance = insurance;
            }
        }

        if (companyInsurance==null) {
            throw new GenericException(String.format("%s does not provide %s", company.getName(), insurehubInsurance.getName()));
        }else {
            NewBusinessClientQuotation clientQuotation = new NewBusinessClientQuotation();
            clientQuotation.setInsurance(insurehubInsurance.getName());

            ClientQuotationCompanyDetails clientQuotationCompanyDetails = new ClientQuotationCompanyDetails();
            clientQuotationCompanyDetails.setCompany(company.getName());
            clientQuotationCompanyDetails.setEmail(company.getEmail());
            clientQuotationCompanyDetails.setMobileNumber(company.getPhoneNumber());

            ClientQuotationPremiumDetails clientQuotationPremiumDetails = new ClientQuotationPremiumDetails();
            BeanUtils.copyProperties(companyInsurance, clientQuotationPremiumDetails);
            clientQuotationPremiumDetails.setId(null);


            NewBusinessClientQuote clientQuote = new NewBusinessClientQuote();
            clientQuote.setCompanyDetails(clientQuotationCompanyDetails);
            clientQuote.setPremiumDetails(clientQuotationPremiumDetails);

            Set<NewBusinessClientQuote> clientQuotes = new HashSet<>();
            clientQuotes.add(clientQuote);
            clientQuotation.setQuotations(clientQuotes);

            if (requestCompanyQuoteDTO.isSendEmail() && requestCompanyQuoteDTO.getEmail() != null) {
                Set<NewBusinessClientQuote> newBusinessClientQuotation = new HashSet<>();
                newBusinessClientQuotation.add(clientQuote);
                sendQuotationEmail(requestCompanyQuoteDTO.getEmail(), insurehubInsurance, newBusinessClientQuotation);
            }

            NewBusinessCompanyQuote companyQuote = new NewBusinessCompanyQuote();
            companyQuote.setCompany(company);
            companyQuote.setEmail(requestCompanyQuoteDTO.getEmail());
            companyQuote.setMobileNumber(requestCompanyQuoteDTO.getMobileNumber());
            companyQuote.setInsurance(insurehubInsurance);
            newBusinessCompanyQuoteRepository.save(companyQuote);

            return clientQuotationRepository.save(clientQuotation);
        }




    }

    private void sendQuotationEmail(String email, InsurehubInsurance insurehubInsurance, Set<NewBusinessClientQuote> quoteSet) {
        StringBuilder content = new StringBuilder();

        if (quoteSet.size() != 0) {
            content.append(String.format("Quotations for the %s are as follows : \n \n", insurehubInsurance.getName()));
            quoteSet.forEach((quotation -> {
                content.append(quotation.getCompanyDetails().getCompany() + "\n");
                content.append(quotation.getCompanyDetails().getMobileNumber() + "\n");
                content.append(quotation.getCompanyDetails().getEmail() + "\n");
                content.append(String.format("Principal Member premium : %s", quotation.getPremiumDetails().getPrincipalMemberPremium() + "\n"));


                if (!insurehubInsurance.getKey().equals("vehicleinsurance") && !insurehubInsurance.getKey().equals("companyvehicleinsurance")) {

                    content.append(String.format("Adult Beneficiary premium : %s", quotation.getPremiumDetails().getAdultBeneficiaryPremium() + "\n"));
                    content.append(String.format("Senior Beneficiary premium : %s", quotation.getPremiumDetails().getSeniorBeneficiaryPremium() + "\n"));
                    content.append(String.format("Child Beneficiary premium : %s", quotation.getPremiumDetails().getChildBeneficiaryPremium() + "\n"));
                }

                content.append("\n");
            }));
        } else {
            content.append(String.format("Could not find any companies providing %s ", insurehubInsurance.getName()));
        }

        log.info(content.toString());
        MultiValueMap<String, Object> formData = new LinkedMultiValueMap<>();

        formData.add("recipient", email);
        formData.add("subject", "Requested Insurance Quotation Results");
        formData.add("body", content.toString());
        utilities.sendEmailWithoutAttachment(formData);


    }


}
