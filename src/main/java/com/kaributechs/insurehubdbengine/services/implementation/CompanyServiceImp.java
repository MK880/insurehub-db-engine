package com.kaributechs.insurehubdbengine.services.implementation;

import com.kaributechs.insurehubdbengine.exceptions.GenericException;
import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.InsurehubInsurance;
import com.kaributechs.insurehubdbengine.models.Template;
import com.kaributechs.insurehubdbengine.models.TemplateBlueprint;
import com.kaributechs.insurehubdbengine.models.dtos.CreateTemplateDTO;
import com.kaributechs.insurehubdbengine.models.dtos.GetCompanyTemplateIdDTO;
import com.kaributechs.insurehubdbengine.models.new_business.CompanyInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.personal_vehicle_insurance.AddCompanyInsuranceDTO;
import com.kaributechs.insurehubdbengine.repositories.CompanyRepository;
import com.kaributechs.insurehubdbengine.repositories.TemplateBlueprintRepository;
import com.kaributechs.insurehubdbengine.repositories.TemplateRepository;
import com.kaributechs.insurehubdbengine.services.ICompanyService;
import com.kaributechs.insurehubdbengine.services.IInsurehubInsuranceService;
import com.kaributechs.insurehubdbengine.utilities.Utilities;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class CompanyServiceImp implements ICompanyService {
    private final CompanyRepository companyRepository;
    private final TemplateBlueprintRepository templateBlueprintRepository;
    private final TemplateRepository templateRepository;

    public CompanyServiceImp(CompanyRepository companyRepository, TemplateBlueprintRepository templateBlueprintRepository, TemplateRepository templateRepository) {
        this.companyRepository = companyRepository;
        this.templateBlueprintRepository = templateBlueprintRepository;
        this.templateRepository = templateRepository;
    }

    @Override
    public Company addCompanyImp(Company company) {
        return companyRepository.save(company);
    }

    @Override
    public List<Company> getAllCompaniesImp() {
        return companyRepository.findAll();
    }

    @Override
    public Company createTemplate(CreateTemplateDTO createTemplateDTO) {
        Company company = getCompanyByIdImp(createTemplateDTO.getCompanyId());
        TemplateBlueprint templateBlueprint = templateBlueprintRepository.findById(createTemplateDTO.getTemplateBlueprintId()).orElseThrow(() -> new GenericException(String.format("TemplateBlueprint with id %s not found", createTemplateDTO.getTemplateBlueprintId())));
        Template template = new Template();
        template.setCompany(company);
        template.setFileName(templateBlueprint.getFileName());
        template.setName(templateBlueprint.getName());
        company.getTemplates().add(template);

        return companyRepository.save(company);
    }

    @Override
    public List<Template> getCompanyTemplatesImp(Long id) {
        Company company = getCompanyByIdImp(id);

        return templateRepository.findAllByCompany(company);
    }

    @Override
    public Company getCompanyByIdImp(Long id) {
        return companyRepository.findById(id).orElseThrow(() -> new GenericException(String.format("Company with id %s not found", id)));

    }

    @Override
    public Company updateCompanyImp(Company company) {
        Company existingCompany = getCompanyByIdImp(company.getId());
        BeanUtils.copyProperties(company,existingCompany, Utilities.getNullPropertyNames(company));
        return companyRepository.save(existingCompany);
    }

    @Override
    public Long getCompanyTemplateIdImp(GetCompanyTemplateIdDTO getCompanyTemplateIdDTO) {
        Long templateId = null;
        Company company = getCompanyByIdImp(getCompanyTemplateIdDTO.getCompanyId());
        if (company.getTemplates()==null || company.getTemplates().isEmpty()) {
            templateId=0L;
        }
        for (Template template :
                company.getTemplates()) {
            if (template.getName().equals(getCompanyTemplateIdDTO.getTemplateName())){
                templateId=template.getId();
                break;
            }else {
                templateId=0L;
            }
        }

        return templateId;

    }


}
