package com.kaributechs.insurehubdbengine.services.implementation;

import com.kaributechs.insurehubdbengine.exceptions.ResourceNotFound;
import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.new_business.ContactDetails;
import com.kaributechs.insurehubdbengine.models.new_business.household_insurance.*;
import com.kaributechs.insurehubdbengine.repositories.new_business.HouseholdInsuranceRepository;
import com.kaributechs.insurehubdbengine.services.ICompanyService;
import com.kaributechs.insurehubdbengine.services.IHouseholdInsuranceService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class HouseholdInsuranceServiceImp implements IHouseholdInsuranceService {
    private final HouseholdInsuranceRepository householdInsuranceRepository;
    private final ICompanyService companyService;

    public HouseholdInsuranceServiceImp(HouseholdInsuranceRepository householdInsuranceRepository, ICompanyService companyService) {
        this.householdInsuranceRepository = householdInsuranceRepository;
        this.companyService = companyService;
    }

    @Override
    public HouseholdInsurance save(HouseholdInsuranceDTO householdInsuranceDTO) {
       HouseholdInsurance householdInsurance= new HouseholdInsurance();
        BeanUtils.copyProperties(householdInsuranceDTO, householdInsurance);

        HouseholdInsuranceMemberDetails membersDetails = new HouseholdInsuranceMemberDetails();
        BeanUtils.copyProperties(householdInsuranceDTO.getMemberDetails(), membersDetails);
        householdInsurance.setMemberDetails(membersDetails);

        ContactDetails contactDetails = new ContactDetails();
        BeanUtils.copyProperties(householdInsuranceDTO.getContactDetails(), contactDetails);
        householdInsurance.setContactDetails(contactDetails);


        Set<HouseholdInsurancePropertyDetails> properties = new HashSet<>();
        if (householdInsuranceDTO.getProperties() != null) {
            householdInsuranceDTO.getProperties().forEach(property->{
                HouseholdInsurancePropertyDetails propertyDetails=new HouseholdInsurancePropertyDetails();
                BeanUtils.copyProperties(property,propertyDetails);

                HouseholdInsurancePropertyAddress propertyAddress=new HouseholdInsurancePropertyAddress();
                BeanUtils.copyProperties(property.getAreaOfPropertyDetails(),propertyAddress);
                propertyDetails.setPropertyAddress(propertyAddress);

                HouseholdInsuranceProtectionDetails protectionDetails=new HouseholdInsuranceProtectionDetails();
                BeanUtils.copyProperties(property.getProtectionDetails(),protectionDetails);
                propertyDetails.setProtectionDetails(protectionDetails);

                propertyDetails.setHouseholdInsurance(householdInsurance);
                properties.add(propertyDetails);
            });
            householdInsurance.setProperties(properties);
        }

        Set<HouseholdInsuranceChildrenDetails> childrenSet=new HashSet<>();
        if (householdInsuranceDTO.getChildren()!=null){
            householdInsuranceDTO.getChildren().forEach(child->{
                HouseholdInsuranceChildrenDetails childrenDetails=new HouseholdInsuranceChildrenDetails();
                BeanUtils.copyProperties(child,childrenDetails);
                childrenDetails.setHouseholdInsurance(householdInsurance);
                childrenSet.add(childrenDetails);
            });
            householdInsurance.setChildren(childrenSet);
        }

        Set<HouseholdInsuranceExtendedFamilyDetails> extendedFamilyDetailsSet=new HashSet<>();
        if (householdInsuranceDTO.getExtendedFamily()!=null){
            householdInsuranceDTO.getExtendedFamily().forEach(member->{
                HouseholdInsuranceExtendedFamilyDetails extendedFamilyDetails=new HouseholdInsuranceExtendedFamilyDetails();
                BeanUtils.copyProperties(member,extendedFamilyDetails);
                extendedFamilyDetails.setHouseholdInsurance(householdInsurance);
                extendedFamilyDetailsSet.add(extendedFamilyDetails);
            });
            householdInsurance.setExtendedFamily(extendedFamilyDetailsSet);
        }

        Set<HouseholdInsuranceBeneficiaryDetails> beneficiaries = new HashSet<>();
        if (householdInsuranceDTO.getBeneficiaries() != null) {
            householdInsuranceDTO.getBeneficiaries().forEach(beneficiary -> {
                HouseholdInsuranceBeneficiaryDetails beneficiaryDetails = new HouseholdInsuranceBeneficiaryDetails();
                        BeanUtils.copyProperties(beneficiary, beneficiaryDetails);
                        beneficiaryDetails.setHouseholdInsurance(householdInsurance);
                        beneficiaries.add(beneficiaryDetails);
                    });
            householdInsurance.setBeneficiaries(beneficiaries);
        }


        HouseholdInsuranceQuestions householdInsuranceQuestions=new HouseholdInsuranceQuestions();
        BeanUtils.copyProperties(householdInsuranceDTO.getInsuranceQuestions(),householdInsuranceQuestions);
        householdInsurance.setInsuranceQuestions(householdInsuranceQuestions);

        Company company=companyService.getCompanyByIdImp(householdInsuranceDTO.getCompanyId());
        householdInsurance.setCompany(company);

        if (company.getHouseholdInsuranceClients() !=null){
            company.getHouseholdInsuranceClients().add(householdInsurance);
        }else{
            Set<HouseholdInsurance> medicalInsurances= new HashSet<>();
            medicalInsurances.add(householdInsurance);
            company.setHouseholdInsuranceClients(medicalInsurances);
        }
        return householdInsuranceRepository.save(householdInsurance);
    }

    @Override
    public HouseholdInsurance getInsuranceByProcessId(String processId) {
        return householdInsuranceRepository.findHouseholdInsuranceByProcessId(processId).orElseThrow(() -> new ResourceNotFound("Life Insurance", "processId", processId));
    }
}
