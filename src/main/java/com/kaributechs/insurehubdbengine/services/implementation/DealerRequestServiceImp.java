package com.kaributechs.insurehubdbengine.services.implementation;

import com.kaributechs.insurehubdbengine.exceptions.ResourceNotFound;
import com.kaributechs.insurehubdbengine.models.dealer.*;
import com.kaributechs.insurehubdbengine.repositories.*;
import com.kaributechs.insurehubdbengine.services.IDealerRequestService;
import com.kaributechs.insurehubdbengine.services.IServicesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Slf4j
public class DealerRequestServiceImp implements IDealerRequestService {
    private final DealerRequestRepository dealerRequestRepository;
    private final DealerServiceRepository dealerServiceRepository;
    private final IServicesService servicesService;


    public DealerRequestServiceImp(DealerRequestRepository dealerRequestRepository, DealerServiceRepository dealerServiceRepository, IServicesService servicesService) {
        this.dealerRequestRepository = dealerRequestRepository;
        this.dealerServiceRepository = dealerServiceRepository;
        this.servicesService = servicesService;
    }

    @Override
    public DealerRequest addDealerRequestImp(DealerRequestQuotationDTO dealerRequestQuotationDTO) {
        log.info("dealer request : {}", dealerRequestQuotationDTO);
        DealerRequest newDealerRequest=new DealerRequest();
        BeanUtils.copyProperties(dealerRequestQuotationDTO,newDealerRequest);


        VehicleDetails vehicleDetails=new VehicleDetails();
        if (dealerRequestQuotationDTO.getVehicleDetails() != null){
            BeanUtils.copyProperties(dealerRequestQuotationDTO.getVehicleDetails(),vehicleDetails);
            newDealerRequest.setVehicleDetails(vehicleDetails);
        }

        ServiceDetails serviceDetails=new ServiceDetails();
        if (dealerRequestQuotationDTO.getServiceDetails() != null) {
            BeanUtils.copyProperties(dealerRequestQuotationDTO.getServiceDetails(),serviceDetails);
            newDealerRequest.setServiceDetails(serviceDetails);
        }

        DriverDetails driverDetails=new DriverDetails();
        if (dealerRequestQuotationDTO.getServiceDetails() != null) {
            BeanUtils.copyProperties(dealerRequestQuotationDTO.getDriverDetails(),driverDetails);
            newDealerRequest.setDriverDetails(driverDetails);
        }

        com.kaributechs.insurehubdbengine.models.dealer.Service requestedService = servicesService.getServiceById(dealerRequestQuotationDTO.getRequestedServiceId());
        newDealerRequest.setRequestedService(requestedService);

        List<DealerService> dealerServices = dealerServiceRepository.findDealerServicesByService(requestedService);
        Set<Quotation> quotations = new HashSet<>();
        dealerServices.forEach(service -> {
            Quotation quotation=new Quotation();
            quotation.setPrice(service.getPrice());
            quotation.setServiceName(service.getName());
            quotation.setDealer(service.getDealer());
            quotation.setDealerRequest(newDealerRequest);
            quotations.add(quotation);
        });
        newDealerRequest.setQuotations(quotations);
        return dealerRequestRepository.save(newDealerRequest);
    }

    @Override
    public DealerRequest getDealerRequestById(Long id) {
        return dealerRequestRepository.findById(id).orElseThrow(()-> new ResourceNotFound("DealerRequest","id",id));
    }

    @Override
    public DealerRequest getDealerRequestByProcessInstanceId(String processId) {
        return dealerRequestRepository.findDealerRequestByProcessId(processId).orElseThrow(()-> new ResourceNotFound("DealerRequest","process id",processId));
    }

    @Override
    public List<DealerRequest> getDealerRequestByUsername(String username) {
        return null;
    }

    @Override
    public List<DealerService> getDealerRequestsByService(com.kaributechs.insurehubdbengine.models.dealer.Service service) {
        return dealerServiceRepository.findDealerServicesByService(service);
    }
}
