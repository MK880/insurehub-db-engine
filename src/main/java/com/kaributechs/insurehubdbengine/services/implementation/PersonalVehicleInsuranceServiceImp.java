package com.kaributechs.insurehubdbengine.services.implementation;

import com.kaributechs.insurehubdbengine.exceptions.ResourceNotFound;
import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.new_business.ContactDetails;
import com.kaributechs.insurehubdbengine.models.new_business.personal_vehicle_insurance.*;
import com.kaributechs.insurehubdbengine.repositories.new_business.PersonalVehicleInsuranceRepository;
import com.kaributechs.insurehubdbengine.services.ICompanyService;
import com.kaributechs.insurehubdbengine.services.IPersonalVehicleInsuranceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@Slf4j
public class PersonalVehicleInsuranceServiceImp implements IPersonalVehicleInsuranceService {
    private final PersonalVehicleInsuranceRepository personalVehicleInsuranceRepository;
    private final ICompanyService companyService;

    public PersonalVehicleInsuranceServiceImp(PersonalVehicleInsuranceRepository personalVehicleInsuranceRepository, ICompanyService companyService) {
        this.personalVehicleInsuranceRepository = personalVehicleInsuranceRepository;
        this.companyService = companyService;
    }

    @Override
    public PersonalVehicleInsurance apply(PersonalVehicleInsuranceDTO personalVehicleInsuranceDTO) {
        PersonalVehicleInsurance personalVehicleInsurance = new PersonalVehicleInsurance();
        BeanUtils.copyProperties(personalVehicleInsuranceDTO, personalVehicleInsurance);

        PersonalDetails personalDetails = new PersonalDetails();
        BeanUtils.copyProperties(personalVehicleInsuranceDTO.getPersonalDetails(), personalDetails);
        personalVehicleInsurance.setPersonalDetails(personalDetails);

        ContactDetails contactDetails = new ContactDetails();
        BeanUtils.copyProperties(personalVehicleInsuranceDTO.getContactDetails(), contactDetails);
        personalVehicleInsurance.setContactDetails(contactDetails);

        Set<PersonalVehicleInsuranceVehicleDetails> vehicles = new HashSet<>();
        if (personalVehicleInsuranceDTO.getVehicles() != null) {
            personalVehicleInsuranceDTO.getVehicles().forEach(vehicle -> {
                PersonalVehicleInsuranceVehicleDetails vehicleDetails = new PersonalVehicleInsuranceVehicleDetails();
                BeanUtils.copyProperties(vehicle, vehicleDetails);
                vehicleDetails.setPersonalVehicleInsurance(personalVehicleInsurance);

                if (vehicle.getSecondaryDriverDetails()!=null) {
                    PersonalInsuranceSecondaryDriverDetails secondaryDriverDetails = new PersonalInsuranceSecondaryDriverDetails();
                    BeanUtils.copyProperties(vehicle.getSecondaryDriverDetails(), secondaryDriverDetails);
                    vehicleDetails.setSecondaryDriverDetails(secondaryDriverDetails);
                }
                vehicles.add(vehicleDetails);


            });
            personalVehicleInsurance.setVehicles(vehicles);

        }


        PersonalVehicleInsuranceQuestions insuranceQuestions = new PersonalVehicleInsuranceQuestions();
        BeanUtils.copyProperties(personalVehicleInsuranceDTO.getInsuranceQuestions(), insuranceQuestions);
        personalVehicleInsurance.setInsuranceQuestions(insuranceQuestions);

        Company company=companyService.getCompanyByIdImp(personalVehicleInsuranceDTO.getCompanyId());
        personalVehicleInsurance.setCompany(company);

        if (company.getPersonalVehicleInsuranceClients() !=null){
            company.getPersonalVehicleInsuranceClients().add(personalVehicleInsurance);
        }else{
            Set<PersonalVehicleInsurance> vehicleInsuranceSet= new HashSet<>();
            vehicleInsuranceSet.add(personalVehicleInsurance);
            company.setPersonalVehicleInsuranceClients(vehicleInsuranceSet);
        }
        return personalVehicleInsuranceRepository.save(personalVehicleInsurance);
    }

    @Override
    public PersonalVehicleInsurance getInsuranceByProcessId(String processId) {
        return personalVehicleInsuranceRepository.findPersonalVehicleInsuranceByProcessId(processId).orElseThrow(() -> new ResourceNotFound("Personal Vehicle Insurance", "processId", processId));
    }
}
