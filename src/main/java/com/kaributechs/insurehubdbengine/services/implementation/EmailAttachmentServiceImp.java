package com.kaributechs.insurehubdbengine.services.implementation;

import com.kaributechs.insurehubdbengine.exceptions.FileStorageException;
import com.kaributechs.insurehubdbengine.exceptions.ResourceNotFound;
import com.kaributechs.insurehubdbengine.models.EmailAttachment;
import com.kaributechs.insurehubdbengine.repositories.EmailAttachmentsRepository;
import com.kaributechs.insurehubdbengine.services.IEmailAttachmentService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class EmailAttachmentServiceImp implements IEmailAttachmentService {
    final EmailAttachmentsRepository emailAttachmentsRepository;


    public EmailAttachmentServiceImp(EmailAttachmentsRepository emailAttachmentsRepository) {
        this.emailAttachmentsRepository = emailAttachmentsRepository;
    }

    @Override
    public EmailAttachment saveEmailAttachment(MultipartFile file) {

            try {
                EmailAttachment emailAttachment=new EmailAttachment();
                emailAttachment.setFilename(file.getOriginalFilename());
                emailAttachment.setType(file.getContentType());
                emailAttachment.setData(file.getBytes());
                return emailAttachmentsRepository.save(emailAttachment);
            } catch (Exception e) {
                throw new FileStorageException("Could not store the file. Error: " + e.getMessage());
            }

    }

    @Override
    public EmailAttachment getEmailAttachmentById(Long id) {
        return emailAttachmentsRepository.findById(id).orElseThrow(()->new ResourceNotFound("Email Template","id",id));
    }
}
