package com.kaributechs.insurehubdbengine.services.implementation;

import com.kaributechs.insurehubdbengine.exceptions.GenericException;
import com.kaributechs.insurehubdbengine.models.TemplateBlueprint;
import com.kaributechs.insurehubdbengine.repositories.TemplateBlueprintRepository;
import com.kaributechs.insurehubdbengine.services.ITemplateBlueprintService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TemplateBlueprintServiceImp implements ITemplateBlueprintService {
    Logger logger= LoggerFactory.getLogger(this.getClass());
    private  final TemplateBlueprintRepository templateBlueprintRepository;


    public TemplateBlueprintServiceImp(TemplateBlueprintRepository templateBlueprintRepository) {
        this.templateBlueprintRepository = templateBlueprintRepository;
    }

    @Override
    public TemplateBlueprint getTemplateBlueprintByIdImp(Long id) {
        return templateBlueprintRepository.findById(id).orElseThrow(()->new GenericException(String.format("TemplateBlueprint with id %s not found",id)));

    }

    @Override
    public TemplateBlueprint addTemplateBlueprint(TemplateBlueprint templateBlueprint) {
        return templateBlueprintRepository.save(templateBlueprint);
    }

    @Override
    public List<TemplateBlueprint> getAllTemplateBlueprintsImp() {
        return templateBlueprintRepository.findAll();
    }

    @Override
    public TemplateBlueprint editTemplateBlueprintImp(TemplateBlueprint blueprint) {
        TemplateBlueprint templateBlueprint=getTemplateBlueprintByIdImp(blueprint.getId());
        templateBlueprint.setFileName(blueprint.getFileName());
        templateBlueprint.setName(blueprint.getName());
        return templateBlueprintRepository.save(templateBlueprint);
    }
}
