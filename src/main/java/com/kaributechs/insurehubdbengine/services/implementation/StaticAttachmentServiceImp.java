package com.kaributechs.insurehubdbengine.services.implementation;

import com.kaributechs.insurehubdbengine.exceptions.AlreadyExists;
import com.kaributechs.insurehubdbengine.exceptions.GenericException;
import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.StaticAttachment;
import com.kaributechs.insurehubdbengine.models.dtos.AddStaticAttachmentDTO;
import com.kaributechs.insurehubdbengine.models.dtos.UpdateStaticAttachmentDTO;
import com.kaributechs.insurehubdbengine.repositories.StaticAttachmentRepository;
import com.kaributechs.insurehubdbengine.services.IStaticAttachmentService;
import com.kaributechs.insurehubdbengine.utilities.Utilities;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class StaticAttachmentServiceImp implements IStaticAttachmentService {
    private final StaticAttachmentRepository staticAttachmentRepository;
    private final Utilities utils;
    private final CompanyServiceImp companyServiceImp;
    Logger logger= LoggerFactory.getLogger(this.getClass());


    public StaticAttachmentServiceImp(StaticAttachmentRepository staticAttachmentRepository, Utilities utils, CompanyServiceImp companyServiceImp) {
        this.staticAttachmentRepository = staticAttachmentRepository;
        this.utils = utils;
        this.companyServiceImp = companyServiceImp;
    }

    @Override
    public StaticAttachment addStaticAttachmentImp(AddStaticAttachmentDTO addStaticAttachmentDTO) throws IOException {

        StaticAttachment staticAttachment=new StaticAttachment();
        Company company=companyServiceImp.getCompanyByIdImp(addStaticAttachmentDTO.getCompanyId());

        company.getStaticAttachments().forEach((attachment)->{
            if (attachment.getName().trim().equals(addStaticAttachmentDTO.getName().trim())){
                throw new AlreadyExists("Static Attachment","Name",attachment.getName());
            }
        });

        staticAttachment.setCompany(company);
        staticAttachment.setName(addStaticAttachmentDTO.getName());
        String extension = FilenameUtils.getExtension(addStaticAttachmentDTO.getAttachment().getOriginalFilename());
        staticAttachment.setFileExtension(extension);
        staticAttachment.setUrl(utils.uploadDocument(utils.convertMultipartFileToFile(addStaticAttachmentDTO.getAttachment()),"Static Attachment"));
        company.getStaticAttachments().add(staticAttachment);

        return staticAttachmentRepository.save(staticAttachment);

    }

    @Override
    public StaticAttachment getStaticAttachmentByIdImp(Long id) {
        logger.info("Fetching static attachment wit id {}",id);
        StaticAttachment staticAttachment= staticAttachmentRepository.findById(id).orElseThrow(()-> new GenericException(String.format("Cannot find static attachment with id %s",id)));
        logger.info("Found static attachment wit id {}",staticAttachment.getId());
        return staticAttachment;
    }

    @Override
    public StaticAttachment updateStaticAttachment(UpdateStaticAttachmentDTO updateStaticAttachmentDTO) throws IOException {
        logger.info(updateStaticAttachmentDTO.toString());
        StaticAttachment staticAttachment=getStaticAttachmentByIdImp(updateStaticAttachmentDTO.getId());
        if (updateStaticAttachmentDTO.getAttachment()!=null){
            staticAttachment.setUrl(utils.uploadDocument(utils.convertMultipartFileToFile(updateStaticAttachmentDTO.getAttachment()),"Static Attachment"));
            String extension = FilenameUtils.getExtension(updateStaticAttachmentDTO.getAttachment().getOriginalFilename());
            staticAttachment.setFileExtension(extension);

        }
        if (updateStaticAttachmentDTO.getName()!=null && !updateStaticAttachmentDTO.getName().equals("")) {
            staticAttachment.setName(updateStaticAttachmentDTO.getName());
        }
        return staticAttachmentRepository.save(staticAttachment);
    }
}
