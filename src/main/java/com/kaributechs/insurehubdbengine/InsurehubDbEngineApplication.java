package com.kaributechs.insurehubdbengine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InsurehubDbEngineApplication {

    public static void main(String[] args) {
        SpringApplication.run(InsurehubDbEngineApplication.class, args);
    }

}
