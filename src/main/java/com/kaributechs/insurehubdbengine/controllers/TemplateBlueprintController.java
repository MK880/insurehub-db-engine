package com.kaributechs.insurehubdbengine.controllers;


import com.kaributechs.insurehubdbengine.models.TemplateBlueprint;
import com.kaributechs.insurehubdbengine.services.ITemplateBlueprintService;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/template/blueprint")
@Hidden
public class TemplateBlueprintController {
    private final ITemplateBlueprintService templateBlueprintService;

    public TemplateBlueprintController(ITemplateBlueprintService templateBlueprintService) {
        this.templateBlueprintService = templateBlueprintService;
    }

    @PostMapping("/add")
    private TemplateBlueprint addCompany(@RequestBody TemplateBlueprint templateBlueprint){
        return templateBlueprintService.addTemplateBlueprint(templateBlueprint);

    }

    @GetMapping("/all")
    private List<TemplateBlueprint> getAllTemplateBlueprints(){
        return templateBlueprintService.getAllTemplateBlueprintsImp();

    }

    @PatchMapping("/update")
    private TemplateBlueprint editTemplateBlueprint(@RequestBody TemplateBlueprint templateBlueprint){
        return templateBlueprintService.editTemplateBlueprintImp(templateBlueprint);
    }

    @GetMapping("/{id}")
    private TemplateBlueprint getTemplateBlueprintById(@PathVariable Long id){
        return templateBlueprintService.getTemplateBlueprintByIdImp(id);

    }
}
