package com.kaributechs.insurehubdbengine.controllers;

import com.kaributechs.insurehubdbengine.models.new_business.medical_insurance.MedicalInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.medical_insurance.MedicalInsuranceDTO;
import com.kaributechs.insurehubdbengine.services.IMedicaIInsuranceService;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/medical-insurance")
@Slf4j
@Hidden
public class MedicalInsuranceController {
    private final IMedicaIInsuranceService medicalInsuranceService;

    public MedicalInsuranceController(IMedicaIInsuranceService medicalInsuranceService) {
        this.medicalInsuranceService = medicalInsuranceService;
    }

    @PostMapping("/save")
    public MedicalInsurance apply(@RequestBody MedicalInsuranceDTO medicalInsuranceDTO){
        log.info("MedicalInsuranceDTO : {}",medicalInsuranceDTO);
        return medicalInsuranceService.save(medicalInsuranceDTO);
    }

    @GetMapping("/get/{processId}")
    public MedicalInsurance getInsuranceByProcessId(@PathVariable String processId){
        log.info("Get Medical Insurance by process id {}",processId);
        return medicalInsuranceService.getInsuranceByProcessId(processId);
    }
}
