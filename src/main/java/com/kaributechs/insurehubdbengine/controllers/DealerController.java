package com.kaributechs.insurehubdbengine.controllers;

import com.kaributechs.insurehubdbengine.models.dealer.Dealer;
import com.kaributechs.insurehubdbengine.models.dealer.AddDealerServiceDTO;
import com.kaributechs.insurehubdbengine.models.dealer.RegisterDealerDTO;
import com.kaributechs.insurehubdbengine.services.IDealerService;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/dealer")
@Hidden
@Slf4j
public class DealerController {
    private final IDealerService dealerService;

    public DealerController(IDealerService dealerService) {
        this.dealerService = dealerService;
    }

    @PostMapping("/register")
    public Dealer registerDealer(@RequestBody RegisterDealerDTO registerDealerDTO){
        log.info("Adding Dealer : {}",registerDealerDTO);
        return dealerService.registerDealer(registerDealerDTO);
    }

    @PostMapping("/service/add")
    public Dealer addDealerService(@RequestBody AddDealerServiceDTO addDealerServiceDTO){
        log.info("Adding Dealer Service: {}", addDealerServiceDTO);
        return dealerService.addDealerService(addDealerServiceDTO);
    }

    @GetMapping("/{username}")
    public Dealer getDealerByUsername( @PathVariable String username){
        return dealerService.getDealerByUsername(username);
    }




}
