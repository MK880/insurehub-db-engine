package com.kaributechs.insurehubdbengine.controllers;

import com.kaributechs.insurehubdbengine.models.insurance_quotation.RequestCompanyQuoteDTO;
import com.kaributechs.insurehubdbengine.models.insurance_quotation.RequestQuoteDTO;
import com.kaributechs.insurehubdbengine.models.insurance_quotation.NewBusinessClientQuotation;
import com.kaributechs.insurehubdbengine.services.IQuotationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/quotation")
@Slf4j
@Tag(name = "Insurance quotation API endpoints", description = "Endpoints for quotations")
public class QuotationController {
    private final IQuotationService quotationService;

    public QuotationController(IQuotationService quotationService) {
        this.quotationService = quotationService;
    }

    @PostMapping("")
    @Operation(summary = "Get insurance quotations")
    private NewBusinessClientQuotation getQuotation(@RequestBody @Valid RequestQuoteDTO requestQuoteDTO){
        return quotationService.getQuote(requestQuoteDTO);
    }

    @PostMapping("/company")
    @Operation(summary = "Get an insurance company quotation")
    private NewBusinessClientQuotation getCompanyQuotation(@RequestBody @Valid RequestCompanyQuoteDTO requestCompanyQuoteDTO){
        return quotationService.getCompanyQuotation(requestCompanyQuoteDTO);
    }
}
