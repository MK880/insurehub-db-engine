package com.kaributechs.insurehubdbengine.controllers;

import com.kaributechs.insurehubdbengine.models.new_business.company_vehicle_insurance.CompanyVehicleInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.company_vehicle_insurance.CompanyVehicleInsuranceDTO;
import com.kaributechs.insurehubdbengine.services.ICompanyVehicleInsuranceService;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/company-vehicle-insurance")
@Slf4j
@Hidden
public class CompanyVehicleInsuranceController {
    private final ICompanyVehicleInsuranceService companyVehicleInsuranceService;

    public CompanyVehicleInsuranceController(ICompanyVehicleInsuranceService companyVehicleInsuranceService) {
        this.companyVehicleInsuranceService = companyVehicleInsuranceService;
    }

    @PostMapping("/apply")
    public CompanyVehicleInsurance apply(@RequestBody @Valid CompanyVehicleInsuranceDTO companyVehicleInsuranceDTO){
        return companyVehicleInsuranceService.apply(companyVehicleInsuranceDTO);
    }

    @GetMapping("/get/{processId}")
    public CompanyVehicleInsurance getInsuranceByProcessId(@PathVariable String processId){
        log.info("Get CompanyVehicleInsurance by process id {}",processId);
        return companyVehicleInsuranceService.getInsuranceByProcessId(processId);
    }
}
