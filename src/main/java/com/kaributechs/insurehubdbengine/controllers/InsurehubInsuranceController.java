package com.kaributechs.insurehubdbengine.controllers;

import com.kaributechs.insurehubdbengine.models.InsurehubInsurance;
import com.kaributechs.insurehubdbengine.models.InsurehubInsuranceDTO;
import com.kaributechs.insurehubdbengine.services.IInsurehubInsuranceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/insurance")
@Slf4j
@Tag(name = "Insurahub insurance API endpoints", description = "Endpoints for insurahub insurance ")
public class InsurehubInsuranceController {
    private final IInsurehubInsuranceService insurehubService;

    public InsurehubInsuranceController(IInsurehubInsuranceService insurehubService) {
        this.insurehubService = insurehubService;
    }

    @PostMapping("/add")
    @Operation(summary = "Add insurehub insurance")
    private InsurehubInsurance addInsurance(@RequestBody @Valid InsurehubInsuranceDTO insurehubInsuranceDTO){
        return insurehubService.addInsurance(insurehubInsuranceDTO);
    }

    @GetMapping("/get/{id}")
    @Operation(summary = "Get insurehub insurance by id")
    private InsurehubInsurance getInsuranceById(@PathVariable Long id){
        return insurehubService.getInsuranceById(id);
    }

    @PatchMapping("/update")
    @Operation(summary = "Update insurehub insurance")
    private InsurehubInsurance updateInsuranceData(@RequestBody InsurehubInsurance insurehubInsurance){
        return insurehubService.updateInsuranceData(insurehubInsurance);
    }

    @GetMapping("/all")
    @Operation(summary = "Get all insurehub insurances")
    private List<InsurehubInsurance> getAllInsurances(){
        return insurehubService.getAllInsurances();
    }


}
