package com.kaributechs.insurehubdbengine.controllers;

import com.kaributechs.insurehubdbengine.models.Template;
import com.kaributechs.insurehubdbengine.models.dtos.EditTemplateDTO;
import com.kaributechs.insurehubdbengine.services.ITemplatesService;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/template")
@Hidden
public class TemplateController {
    private final ITemplatesService templatesService;


    public TemplateController(ITemplatesService templatesService) {
        this.templatesService = templatesService;
    }

    @PatchMapping("/update")
    public Template editTemplate( @RequestParam("templateId") Long templateId,@RequestParam("headerImageUrl") String headerImageUrl, @RequestParam("footerImageUrl") String footerImageUrl, @RequestParam("signature") String signature) {
        return templatesService.editTemplateImp(new EditTemplateDTO(templateId, headerImageUrl, footerImageUrl,signature));
    }

    @GetMapping("/{id}")
    public Template getTemplateById(@PathVariable Long id) {
        return templatesService.getTemplateByIdImp(id);
    }
}
