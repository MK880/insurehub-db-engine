package com.kaributechs.insurehubdbengine.controllers;

import com.kaributechs.insurehubdbengine.models.new_business.household_insurance.HouseholdInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.household_insurance.HouseholdInsuranceDTO;
import com.kaributechs.insurehubdbengine.models.new_business.life_insurance.LifeInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.life_insurance.LifeInsuranceDTO;
import com.kaributechs.insurehubdbengine.services.IHouseholdInsuranceService;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/household-insurance")
@Slf4j
@Hidden
public class HouseholdInsuranceController {
    private final IHouseholdInsuranceService householdInsuranceService;

    public HouseholdInsuranceController(IHouseholdInsuranceService householdInsuranceService) {
        this.householdInsuranceService = householdInsuranceService;
    }

    @PostMapping("/save")
    public HouseholdInsurance apply(@RequestBody @Valid HouseholdInsuranceDTO householdInsuranceDTO){
        return householdInsuranceService.save(householdInsuranceDTO);
    }

    @GetMapping("/get/{processId}")
    public HouseholdInsurance getInsuranceByProcessId(@PathVariable String processId){
        log.info("Get Household Insurance by process id {}",processId);
        return householdInsuranceService.getInsuranceByProcessId(processId);
    }
}
