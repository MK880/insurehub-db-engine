package com.kaributechs.insurehubdbengine.controllers;

import com.kaributechs.insurehubdbengine.models.new_business.life_insurance.LifeInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.life_insurance.LifeInsuranceDTO;
import com.kaributechs.insurehubdbengine.services.ILifeInsuranceService;
import com.kaributechs.insurehubdbengine.services.IPersonalVehicleInsuranceService;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/life-insurance")
@Slf4j
@Hidden
public class LifeInsuranceController {
    private final ILifeInsuranceService lifeInsuranceService;

    public LifeInsuranceController(ILifeInsuranceService lifeInsuranceService) {
        this.lifeInsuranceService = lifeInsuranceService;
    }

    @PostMapping("/save")
    public LifeInsurance apply(@RequestBody @Valid LifeInsuranceDTO lifeInsuranceDTO){
        return lifeInsuranceService.save(lifeInsuranceDTO);
    }

    @GetMapping("/get/{processId}")
    public LifeInsurance getInsuranceByProcessId(@PathVariable String processId){
        log.info("Get Life Insurance by process id {}",processId);
        return lifeInsuranceService.getInsuranceByProcessId(processId);
    }
}
