package com.kaributechs.insurehubdbengine.controllers;

import com.kaributechs.insurehubdbengine.models.dealer.Service;
import com.kaributechs.insurehubdbengine.models.dtos.ServiceDTO;
import com.kaributechs.insurehubdbengine.services.IServicesService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/services")
@Slf4j
@Tag(name = "Dealer services API endpoints", description = "Endpoints for dealer services")
public class ServiceController {
    private final IServicesService servicesService;

    public ServiceController(IServicesService servicesService) {
        this.servicesService = servicesService;
    }

    @PostMapping("/add")
    @Operation(summary = "Add dealer service")
    public Service addService(@RequestBody ServiceDTO serviceDTO){
        return servicesService.addService(serviceDTO);
    }

    @GetMapping("/get/{id}")
    @Operation(summary = "Get dealer service by id")
    public Service addService(@PathVariable Long id){
        return servicesService.getServiceById(id);
    }

    @PatchMapping("/update")
    @Operation(summary = "Update dealer service")
    public Service updateService(@RequestBody ServiceDTO serviceDTO){
        return servicesService.updateService(serviceDTO);
    }

    @DeleteMapping("/delete/{id}")
    @Operation(summary = "Delete dealer service by id")
    public  void deleteServiceById(@PathVariable Long id){
         servicesService.deleteServiceById(id);
    }

    @GetMapping("/all")
    @Operation(summary = "Get all dealer services")
    public List<Service> addService(){
        return servicesService.getAllServices();
    }


}
