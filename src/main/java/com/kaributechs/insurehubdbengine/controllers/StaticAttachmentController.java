package com.kaributechs.insurehubdbengine.controllers;

import com.kaributechs.insurehubdbengine.models.StaticAttachment;
import com.kaributechs.insurehubdbengine.models.dtos.AddStaticAttachmentDTO;
import com.kaributechs.insurehubdbengine.models.dtos.UpdateStaticAttachmentDTO;
import com.kaributechs.insurehubdbengine.services.IStaticAttachmentService;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api/v1")
@Hidden
public class StaticAttachmentController {
    private final IStaticAttachmentService staticAttachmentService;

    public StaticAttachmentController(IStaticAttachmentService staticAttachmentService) {
        this.staticAttachmentService = staticAttachmentService;
    }

    @PostMapping("/company/add/static-attachment")
    public StaticAttachment addStaticAttachment(@RequestParam("companyId") Long companyId, @RequestParam("attachment")MultipartFile attachment, @RequestParam("name") String name) throws IOException {
        AddStaticAttachmentDTO addStaticAttachmentDTO=new AddStaticAttachmentDTO(companyId,attachment,name.trim());
        return staticAttachmentService.addStaticAttachmentImp(addStaticAttachmentDTO);
    }

    @GetMapping("/static-attachment/{id}")
    public StaticAttachment getStaticAttachmentById(@PathVariable Long id){
        return staticAttachmentService.getStaticAttachmentByIdImp(id);

    }

    @PatchMapping("static-attachment/update")
    public StaticAttachment updateStaticAttachment(@RequestParam("id") Long id, @RequestParam(required = false) MultipartFile attachment, @RequestParam(required = false) String name) throws IOException {
        UpdateStaticAttachmentDTO updateStaticAttachmentDTO=new UpdateStaticAttachmentDTO();
        updateStaticAttachmentDTO.setId(id);
        updateStaticAttachmentDTO.setName(name.trim());
        if (attachment!=null && !attachment.isEmpty()){
            updateStaticAttachmentDTO.setAttachment(attachment);
        }else updateStaticAttachmentDTO.setAttachment(null);
        return staticAttachmentService.updateStaticAttachment(updateStaticAttachmentDTO);
    }
}
