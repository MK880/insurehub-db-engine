package com.kaributechs.insurehubdbengine.controllers;

import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.Template;
import com.kaributechs.insurehubdbengine.models.dtos.CreateTemplateDTO;
import com.kaributechs.insurehubdbengine.models.dtos.GetCompanyTemplateIdDTO;
import com.kaributechs.insurehubdbengine.services.ICompanyService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/company")
@Tag(name = "Company API endpoints", description = "Endpoints for companies")
public class CompanyController {

    private final ICompanyService companyService;

    public CompanyController(ICompanyService companyService) {
        this.companyService = companyService;
    }

    @PostMapping("/add")
    @Operation(summary = "Add company / Register company")
    public Company addCompany(@RequestBody Company company){
        return companyService.addCompanyImp(company);

    }

    @GetMapping("/{id}/templates")
    @Operation(summary = "Get company email templates")
    public List<Template> getCompanyTemplates(@PathVariable Long id){
        return companyService.getCompanyTemplatesImp(id);

    }

    @GetMapping("/all")
    @Operation(summary = "Get all companies")
    public List<Company> getAllCompanies(){
        return companyService.getAllCompaniesImp();
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get company by id")
    public Company getCompany(@PathVariable Long id){
        return companyService.getCompanyByIdImp(id);
    }


    @PostMapping("/create/template")
    @Operation(summary = "Create company template")
    public Company addCompany(@RequestBody CreateTemplateDTO createTemplateDTO){
        return companyService.createTemplate(createTemplateDTO);

    }

    @PatchMapping("/update")
    @Operation(summary = "Update company")
    public Company editCompany(@RequestBody Company company){
        return companyService.updateCompanyImp(company);
    }


    @PostMapping("/template/id")
    @Operation(summary = "Get company template id")
    public Long getCompanyTemplateId(@RequestBody GetCompanyTemplateIdDTO getCompanyTemplateIdDTO){
        return companyService.getCompanyTemplateIdImp(getCompanyTemplateIdDTO);

    }






}
