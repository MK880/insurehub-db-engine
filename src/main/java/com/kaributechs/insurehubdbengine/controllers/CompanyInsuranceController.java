package com.kaributechs.insurehubdbengine.controllers;

import com.kaributechs.insurehubdbengine.models.Company;
import com.kaributechs.insurehubdbengine.models.new_business.CompanyInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.UpdateCompanyInsuranceDTO;
import com.kaributechs.insurehubdbengine.models.new_business.personal_vehicle_insurance.AddCompanyInsuranceDTO;
import com.kaributechs.insurehubdbengine.services.ICompanyInsuranceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/company-insurance")
@Tag(name = "Company Insurance API endpoints", description = "Endpoints for company insurance")
public class CompanyInsuranceController {
    private final ICompanyInsuranceService companyInsuranceService;


    public CompanyInsuranceController(ICompanyInsuranceService companyInsuranceService) {
        this.companyInsuranceService = companyInsuranceService;
    }


    @PostMapping("/add")
    @Operation(summary = "Add company insurance")
    public Company getCompanyTemplateId(@RequestBody AddCompanyInsuranceDTO addCompanyInsuranceDTO){
        return companyInsuranceService.addCompanyInsurance(addCompanyInsuranceDTO);
    }

    @GetMapping("/get/{id}")
    @Operation(summary = "Get company insurance by id")
    public CompanyInsurance getCompanyInsuranceById(@PathVariable Long id){
        return companyInsuranceService.getCompanyInsuranceById(id);
    }

    @PatchMapping("/update")
    @Operation(summary = "Update company insurance")
    public CompanyInsurance updateCompanyInsurance(@RequestBody @Valid UpdateCompanyInsuranceDTO updateCompanyInsuranceDTO){
        return companyInsuranceService.updateCompanyInsurance(updateCompanyInsuranceDTO);
    }
}
