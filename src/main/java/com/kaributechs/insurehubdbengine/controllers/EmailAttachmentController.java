package com.kaributechs.insurehubdbengine.controllers;

import com.kaributechs.insurehubdbengine.models.EmailAttachment;
import com.kaributechs.insurehubdbengine.services.IEmailAttachmentService;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/v1/attachment")
@Hidden
public class EmailAttachmentController {
    private final IEmailAttachmentService emailAttachmentService;

    public EmailAttachmentController(IEmailAttachmentService emailAttachmentService) {
        this.emailAttachmentService = emailAttachmentService;
    }

    @PostMapping("/save")
    public EmailAttachment saveEmailAttachment(@RequestParam(required = true) MultipartFile attachment){
        return emailAttachmentService.saveEmailAttachment(attachment);
    }

    @GetMapping("/{id}")
    public EmailAttachment getEmailAttachment(@PathVariable Long id){
        return emailAttachmentService.getEmailAttachmentById(id);
    }
}
