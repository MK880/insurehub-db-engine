package com.kaributechs.insurehubdbengine.controllers;

import com.kaributechs.insurehubdbengine.models.new_business.company_vehicle_insurance.CompanyVehicleInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.company_vehicle_insurance.CompanyVehicleInsuranceDTO;
import com.kaributechs.insurehubdbengine.models.new_business.personal_vehicle_insurance.PersonalVehicleInsurance;
import com.kaributechs.insurehubdbengine.models.new_business.personal_vehicle_insurance.PersonalVehicleInsuranceDTO;
import com.kaributechs.insurehubdbengine.services.ICompanyVehicleInsuranceService;
import com.kaributechs.insurehubdbengine.services.IPersonalVehicleInsuranceService;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/personal-vehicle-insurance")
@Slf4j
@Hidden
public class PersonalVehicleInsuranceController {
    private final IPersonalVehicleInsuranceService personalVehicleInsuranceService;

    public PersonalVehicleInsuranceController(IPersonalVehicleInsuranceService personalVehicleInsuranceService) {
        this.personalVehicleInsuranceService = personalVehicleInsuranceService;
    }

    @PostMapping("/apply")
    public PersonalVehicleInsurance apply(@RequestBody PersonalVehicleInsuranceDTO personalVehicleInsuranceDTO){
        return personalVehicleInsuranceService.apply(personalVehicleInsuranceDTO);
    }

    @GetMapping("/get/{processId}")
    public PersonalVehicleInsurance getInsuranceByProcessId(@PathVariable String processId){
        log.info("Get Personal Vehicle Insurance by process id {}",processId);
        return personalVehicleInsuranceService.getInsuranceByProcessId(processId);
    }
}
