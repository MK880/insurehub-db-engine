package com.kaributechs.insurehubdbengine.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.EXPECTATION_FAILED)
public class FileStorageException extends RuntimeException{
    public FileStorageException(String message){
        super(message);
    }
}
