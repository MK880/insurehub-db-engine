package com.kaributechs.insurehubdbengine.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class AlreadyExists extends RuntimeException {

    public AlreadyExists(String resource, String field, String value){
        super(String.format("%s with %s \"%s\" already Exists",resource,field,value));
    }


}
