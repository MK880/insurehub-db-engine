package com.kaributechs.insurehubdbengine.exceptions;

import lombok.Data;

@Data
public class ServiceNotFoundException extends RuntimeException{
    private final Long serviceId;
    public ServiceNotFoundException( Long serviceId) {
        super(String.format("Service not found with id : '%d'",serviceId));
        this.serviceId = serviceId;
    }
}
