package com.kaributechs.insurehubdbengine.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class WrongFormatException extends RuntimeException{
    public WrongFormatException(String format) {
        super("Wrong file format supplied.Allowed format : "+format);
    }
}
