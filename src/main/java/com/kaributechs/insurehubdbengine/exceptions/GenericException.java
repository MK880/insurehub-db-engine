package com.kaributechs.insurehubdbengine.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class GenericException extends RuntimeException {
    public GenericException(String message) {
        super("Error From DB Engine : "+message);
    }
}
