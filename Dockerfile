FROM openjdk:11
MAINTAINER tinojobi@gmail.com
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} insurehub-db-engine.jar
ENTRYPOINT ["java","-Dspring.profiles.active=prod","-jar","/insurehub-db-engine.jar"]